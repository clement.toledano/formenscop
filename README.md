# FORMENSCOP BEWEB 

## Description
Développement d'un environnement d'administration interne pour un centre de formation.
Ce projet est basé sur l'inter-connection de quatre Api reliées à une même BDD:
- **Salarié** 
- **Stagiaire** 
- **Gestion RH**
- **Vers l'emploi**

Le point d'entrée de l'application est la page de login. 
Chaque utilisateur a accés, selon son rôle, à une api.

## Outils de développement:
### Back-end
- Framework PHP MVC
- Base de données MySql

### Front-end
- JQuery pour les librairies fullcalendarJs, Moment.js, JQuery-ui
- Template admin avec Bootstrap 4

## Installation
1. Clonez les dépôts de chaque api dans un dossier a la racine de votre serveur web.
- git clone https://gitlab.com/beweb-beziers-03/bzr_common_formenscop.git
- git clone https://gitlab.com/beweb-beziers-03/bzr_formenscop_salarie.git
- git clone https://gitlab.com/beweb-beziers-03/bzr_formenscop_trainee.git
- git clone https://gitlab.com/beweb-beziers-03/bzr_formenscop_gestionrh.git
2. Mettez en place les virtualhosts:
    - Configuration Apache2 dans votre terminal:  
    `nano /etc/hosts  `  
    `127.0.0.1       common.formenscop.bwb  `  
    `127.0.0.1       gestionrh.formenscop.bwb  `  
    `127.0.0.1       stagiaire.formenscop.bwb  `  
    `127.0.0.1       salarie.formenscop.bwb  `  
    Sauvegardez (Ctrl + o entrée Ctrl + x)

    `nano /etc/apache2/sites-available/000-default.conf`
    ```
    <VirtualHost *:80>
            ServerName common.formenscop.bwb
            DocumentRoot /var/www/html/serveurweb/formenscop/common
            <Directory /var/www/html/serveurweb/formenscop/common>
            </Directory>
    </VirtualHost>
    ```
    Faites de même pour chaque application
    Sauvegardez (Ctrl + o entrée Ctrl + x)

    Redémarrez le serveur:
    `service apache2 restart`

3. Installez les dépendances avec la commande : composer install
4. Configurez les accés en base de données dans le fichier database.json
5. Requêtes http qui correspondent à chaque api:
- Mot de passe: 1234
- Salarié http://salarie.formenscop.bwb/
    - Salarié coordinateur: ocopeland8@google.es 
    - Salarié formateur: ptippings6@bbb.org 
    - Salarié accompagnateur: emcguigan5@g.co 
- Stagiaire http://stagiaire.formenscop.bwb/
    - login stagiaire en formation: hguarin1@prlog.org 
    - login stagiaire en accompagnement: bpalfery4@dagondesign.com 
- Gestion RH http://gestionrh.formenscop.bwb/
    - login: wmacgillivray0@macromedia.com 
- Vers l'emploi http://emploi.formenscop.bwb/
    - login: cyitzhakof9@storify.com 
6. Installation de la BDD:
    - Dans MySql, créez une nouvelle base de données nommée "formenscop"
    - Cliquez sur "import" pour récupérer la BDD située dans le dossier: formenscop/rh/bdd/ et executez

### Organisation de chaque Api
- L'application « **Salarié** » permet de gérer son planning, remplir
  des demandes de congés, interagir avec les ressources humaines à sa charge,
  interagir avec les organismes externes.

- L'application « **Stagiaire** » permet d’avoir accès a son planning, a ses
  RDV avec ses interlocuteurs au sein de FORMENSCOP. Il dispose de toutes
  les ressources disponibles dans le centre.

- L'application « **Gestion RH** » a pour but de centraliser les actions de
  formation et d’accompagnement, les affectations des différents salariés. De
  traiter les demandes de congés.

- L'application « **Vers l’emploi** » permet aux entreprises de trouver les
  compétences dont dispose FORMENSCOP, ou de poster des offres d’emploi
  sur la plateforme (tout types d’offre).

- Le dossier "common" contient le système de messagerie et le calendrier
qui sont tous deux utilisés par toutes les applications.

**ATTENTION !!!** 
Afin de ne pas avoir des erreurs de type *Cors policy*, générées par les requêtes faites vers 
la messagerie ou le calendrier (car situés sur un nom de domaine différent), 
il est nécessaire d'effectuer les opérations suivantes dans votre terminal:
`nano /etc/apache2/sites-enabled/000-default.conf  `
Rajoutez les lignes situées entre les blises <Directory>
```
<VirtualHost *:80>
       ServerName formenscop.bwb
       DocumentRoot /var/www/html/serveurweb/devFormenscop/common
               <Directory "/var/www/html/serveurweb/devFormenscop/common/">
                    Require all granted
                    AllowOverride all
                    Header set Access-Control-Allow-Origin "*"
                    Header set Access-Control-Allow-Methods "POST, GET, OPTIONS, DELETE, PUT"
               </Directory>
</VirtualHost>
```
Sauvegardez (Ctrl + o entrée Ctrl + x)

    Redémarrez le serveur:
    `service apache2 restart`

## Structure  

1. Le dossier assets/ 
Contient les fichiers Javascript (requêtes Ajax), les images et le CSS
 
2. Le dossier config/   
Contient les fichiers de configuration :
 * database.json : configuration de la base de données
 * routing.json : configuration du mapping entre les routes et les controleurs

3. Le dossier controllers/   
Contient les controleurs qui seront invoqués lors des requêtes http

4. Le dossier core/
Contient le routing, le sécurity middleware, 
l'abstract DAO qui est la configuration de la connection à la BDD

5. Le dossier dao/   
Contient les fichiers DAO pour l'accés aux données

6. Le dossier models/   
Contient les entités correspondantes à la logique métier de l'application

7. Le dossier vendor/   
Contient les dépendances récupérées via composer

8. Le dossier views/   
Contient les vues de l'application

9. A la racine   
 * le fichier index.php qui est le point d'entrée de l'application
 * le fichier composer.json qui est le fichier de configuration de composer
 * le fichier .htaccess qui override la configuration d'apache (assurez vous d'avoir le mod_rewrite activé et opérationnel)
 * le fichier .gitignore qui exclu des commit le dossier vendor et le fichier composer.lock (fichiers générés par composer) 





