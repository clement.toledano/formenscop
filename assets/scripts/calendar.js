//* au chargement de la page 'calendar.php'
$(document).ready(function () {
    //todo userId = valeur de l'input caché ($_SESSION['id'])
    let userId = $('.idHide').text();
    console.log(userId);
    //* selectionne la div id="calendar" et permet d'afficher la grille du calendrier
    $('#calendar').fullCalendar({
      themeSystem: 'bootstrap4',
      
      //* correspond aux éléments affichés en haut de la grille du calendrier
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      //* permet d'afficher les events sur le calendrier
      events: [],
      //* différente option pour l'event
      editable: true,
      droppable: true,
      selectable: true,
      selectHelper: true,
      
      
      //! AU CLIC SUR UN EVENT  MODALE POUR UPDATE ou VISUEL (selon droit)
      eventClick: function(event) {
        $('#eventannul').hide();
        $('#eventvalidate').hide();
        let id = event.id;
        //* au clic sur un event ouvre la modal pour update l'event !!
        $('.fc-content').attr("data-toggle", "modal");
        $('.fc-content').attr("data-target", "#modal-update");
        
        //* pré-rempli les champs de la modal avec les differentes valeurs de l'event
        //* requête ajax pour récuperer toutes les infos d'un event
        $.ajax({
          url: "http://formenscop.bwb/calendar",
          method: "POST",
          dataType: "JSON",
          data: {
            id: id
          },
          success: function (result) {
            //* pré-rempli les champs de la modale avec les valeurs stockées en bdd reçues en result de la requête
            $('#nameEvent').attr("value", result[0].title);
            $('#descriptionEvent').text(result[0].description);
            $('#startTimeEvent').attr('value', result[0].start)
            $('#endTimeEvent').attr("value", result[0].end);
            //* modifie le titre de la modale en fonction du type d'event et du creator
            $('#EventTilteUpdate').text("Type d'event: " + result[0].type + " - Crée par: " + result[0].firstName)
            //* requête ajax pour récuperer toutes les invités d'un event
            let id = event.id;
            $.ajax({
              url: `http://formenscop.bwb/calendar/guests`,
              method: "POST",
              data: {
                id: id
              },
              
              success: function (res) {
                let guests = JSON.parse(res);
                //* vide la div ou sont affichés les nomns des invités
                $('.displayGuestupdate').empty();
                //* parcourir le tableau pour afficher les élements au bon endroit !!
                $('.displayGuestupdate').append('<h5>Invités</h5>')
                for (let i = 0; i < guests.length; i++) {
                  let idguest = guests[i].Account_id_guest;
                  let state = guests[i].state;
                  let icone;
                  if(state === 'no'){
                    icone = '<i class="fas fa-times"></i>';
                  } else if (state ==='maybe'){
                    icone = '<i class="fas fa-question"></i>'
                  }else{
                    icone = '<i class="fas fa-check"></i>';
                  }
                  let val = $('#guestsupdate').val();
                    $('.displayGuestupdate').append("<div id=update" + idguest + " class='divguest'>" + guests[i].FirstName+ "  "+icone+" </div>");
                    $('#guestsupdate').val(val + '/' + idguest + '/');
                    $('#validateButton').hide();
                     $('#refuseButton').hide();
                
                  //* si le créateur de l'event n'est pas l'utilisateur connécté
                  if (parseInt(idguest) == parseInt(userId)) {
                    console.log('different');
                    //* grise les champs et cache les bouton d'update et delete
                    $('#nameEvent').prop('disabled', true);
                    $('#descriptionEvent').prop('disabled', true);
                    $('#startTimeEvent').prop('disabled', true);
                    $('#endTimeEvent').prop('disabled', true);
                    $('#updateButton').hide();
                    $('#deleteButton').hide();
  
  
                   
                    //* si le cteateur de l'event est l'utilisateur connécté    
                } else {
                  
               // $('.displayGuestupdate').empty();
                   
                    console.log('creator')
                   //!
                       //! AU CLIC SUR UPDATE BUTTON
            $('#updateButton').click(function () {
              //* requête ajax avec les données neccessaire a l'update
              //* alert($('#guestsupdate').val());
              $.ajax({
                method: 'PUT',
                url: `http://formenscop.bwb/calendar/update`,
                data: {
                  'id': event.id,
                  'start': $('#startTimeEvent').val(),
                  'end': $('#endTimeEvent').val(),
                  'title': $('#nameEvent').val(),
                  'description': $('#descriptionEvent').val(),
                  'creator': userId,
                  'guests': $('#guestsupdate').val(),
                  'type': result[0].type
                },
                success: function (result) {
                  alert(result);
              //     //todo modal success si result != 'erreur'
              //     //todo modal failed si result == 'erreur'
                }
                
              })
            })
             //! AU CLIC SUR  DELETE BUTTON
             $('#deleteButton').click(function () {
              
              $.ajax({
                url: "http://formenscop.bwb/calendar/delete",
                method: "POST",
                data: {
                  'id': event.id,
                  'userId': userId
                },
                success: function (result) {
                  alert(result);
                  //todo modal success si result != empty
                  //todo modal failed si result == empty
                }
              })
            })
                }
               
                }
              }
            });
          
          
           
          }
        })
      },
  
      
      //* permet de recuperer et afficher les events via une method du controller
      eventSources: [
        
        {
          url: `http://formenscop.bwb/calendar/modules/${userId}`,
          method: 'GET',
          dataType: "JSON",
          failure: function () {
            alert('there was an error while fetching events!');
          },
          //* option -> si plusieurs types d'events permet de les affichés de differentes couleurs...
          color: 'pink', // a non-ajax option
          textColor: 'black' // a non-ajax option
        },
        
        {
          url: `http://formenscop.bwb/calendar/autres/${userId}`,
          method: 'GET',
          dataType: "JSON",
          failure: function () {
            alert('there was an error while fetching events!');
          },
          //* option -> si plusieurs types d'events permet de les affichés de differentes couleurs...
          color: 'blue', // a non-ajax option
          textColor: 'black' // a non-ajax option
        },
        
        {
          url: `http://formenscop.bwb/calendar/rdv/${userId}`,
          method: 'GET',
          dataType: "JSON",
          failure: function () {
            alert('there was an error while fetching events!');
          },
          //* option -> si plusieurs types d'events permet de les affichés de differentes couleurs...
          color: 'orange', // a non-ajax option
          textColor: 'black' // a non-ajax option
        }
        
        // any other sources... permet d'ajouter plusieurs sources donc recuperer les events en fonction des types (perso, formation, rdv entreprise...
      ]
      
    });
  
    //! #########  CREATE EVENT
    
    //! AJOUT DES INVITÉS A UN EVENT (voir code html pour les id et class)
    $('#addGuest').click(function () {
      let id = $('#select').val();
      let val = $('#guests').val();
      
      $('#guests').val(val + '/' + id + '/');
      if (($('#' + id).html()) != null) {
        $('.displayGuest').append("<div id=" + id + " class='divguest'>" + $('#' + id).html() + " </div>");
      }
      $('#select').val("");
      $('#' + id).hide();
      
      //! ENLEVER DES INVITÉS
      $('.divguest').click(function () {
        let id = $(this).attr('id');
        $('#' + id).show();
        let guest = $('#guests').val();
        $('#guests').val(guest.replace('/' + id + '/', ''));
        $(this).remove();
      })
    })
    //! CREATE EVENT (AU CLIC SUR LE BOUTON DE LA MODALE)
    
    $('#createEvent').click(function () {
      let userId = $('.idHide').text();
    
      let form = $('#createForm').serialize();
     
      $.ajax({
        url: "http://formenscop.bwb/calendar/create",
        method: 'POST',
        data: form,
        success: function (result) {
          alert(result);
        },
        error: function (err) {
          console.log(err);
        }
      })
    })
  });
  