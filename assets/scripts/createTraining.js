//* quand la page est chargée et au click sur le bouton connect
$().ready(function () {
    $('#button_create_training').click(function () {
        //* recupère les données du formulaire dans une variable
        var form_data = $('#create_training_form').serialize();
        //* requête ajax en POST vers la page qui gère la connexion + data_form
        $.ajax({
            method: "POST",
            url: "/training/",
            data: form_data,
            //* si la requête réussi 

            success: function (result) {
                //alert(result)
                if (result === 'true') {
                    $('#response').html(
                        "<div class='alert alert-success'>Formation créé avec succès !  </div>")
                    $('#button_create_training').html(
                        "Enregistrement <img width='20' height='20' src='assets/images/loadeer.gif' alt='loader'>")


                    setTimeout(
                        function () {
                            window.location.href = "/trainings";
                        }, 2000);

                } else {
                    $('#response').html(
                        "<div class='alert alert-danger'>Il y a un probleme !!</div>")
                }
            },

            error: function () {
                window.location.replace("/404");
            }

        })
    })

});