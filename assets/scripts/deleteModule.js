$(document).ready(function () {
    $("#button_delete_module").click(function () {
        var form_data = $('#delete_module_form').serialize();
        $.ajax({
            type: "Post",
            url: "/module/",
            data:form_data,
            success: function (result) {
                if (result === "true") {
                    $('#response').html(
                        "<div class='alert alert-success'>Module supprimé avec succès !  </div>")
                        $('#button_delete_role').html(
                            "Suppression <img width='20' height='20' src='assets/images/loadeer.gif' alt='loader'>")
                    setTimeout(
                        function () {
                            window.location.href = "/modules";
                        }
                        , 2000);
                } else {
                    $('#response').html(
                        "<div class='alert alert-danger'>le module n'existe pas.</div>")
                }
            },
            // error response will be here
            error: function (xhr, resp, text) {
                console.log("ajax error - 404 not found");
                // on error, tell the user login has failed & empty the input boxes
                $('#response').html(
                    "<div class='alert alert-danger'>Login failed. Email or password is incorrect.</div>"
                );
                //login_form.find('input').val('');
            }

        })
    })

})