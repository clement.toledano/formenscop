$(document).ready(function () {
    $("#button_delete_role").click(function () {
        var form_data = $('#delete_role_form').serialize();
        //alert(form_data);
        $.ajax({
            type: "Post",
            url: "/role/",
            data:form_data,
            success: function (result) {
                if (result === "true") {
                    $('#response').html(
                        "<div class='alert alert-success'>Role supprimé avec succès !  </div>")
                        $('#button_delete_role').html(
                            "Suppression <img width='20' height='20' src='assets/images/loadeer.gif' alt='loader'>")
                    setTimeout(
                        function () {
                            window.location.href = "/role";
                        }
                        , 2000);
                } else {
                    $('#response').html(
                        "<div class='alert alert-danger'>le role n'existe pas.</div>")
                }
            },
            // error response will be here
            error: function (xhr, resp, text) {
                console.log("ajax error - 404 not found");
                // on error, tell the user login has failed & empty the input boxes
                $('#response').html(
                    "<div class='alert alert-danger'>Login failed. Email or password is incorrect.</div>"
                );
                //login_form.find('input').val('');
            }

        })
    })

})