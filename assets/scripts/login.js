
 //* quand la page est chargée et au click sur le bouton connect
 $().ready(function () {
     $('#button_login').click(function () {
         //* recupère les données du formulaire dans une variable
         var form_data = $('#form_login').serialize();
         //console.log(formdata);


         //* requête ajax en POST vers la page qui gère la connexion + data_form
         $.ajax({
             method: "POST",
             url: "/login",
             data: form_data,
             //* si la requête réussi 
             success: function (result) {
                //alert(result); 
                //* si le resultat de la requête est succes login
                 if (result === 'true') {
                     window.location.replace("/home");
                     //* sinon renvoi un message d'erreur - 'login failed'

                 } else if (result === 'false') {
                     $('#response').html(
                         "<div class='alert alert-danger'>L'email ou le mot de passe est incorrect.</div>")
                 }
             },
             
             error: function () {
                 window.location.replace("/404");
             }

         })
     })

 });