/**Quand la page est chargée, récupère l'id de l'utilisateur stocké dans header.php (ligne 20)*/
$().ready(function () {
    $('.idHide').hide();
    let id = $('.idHide').text();
    $('#newevent').empty();

    /**  Vérification si nouveaux events*/
    //* requête vers l'api commune pour vérifier si il y a de nouveaux events
    $.ajax({
        method: 'GET',
        url: `http://formenscop.bwb/event/viewnew/${id}`,
        dataType: "JSON",
        success: function (result) {
            //alert(result);
            if (result == 'NOnews') {
                //* si il n'y a pas de nouvel event, desactive le bouton event et enlève "le point rouge"
                $('#button_event').attr("data-toggle", "");
                $('#event').attr("class", "");
            } else {
                //* sinon il y a de nouveaux events, requête vers l'api commune pour les récuperer au clic sur l'icone "event"
                $('#button_event').click(function () {
                    $('#newevent').html('new event');
                    let newEvents = result.newEvent;
                    //* Pour chaque type d'events, affichage dans la liste déroulante
                    for (event of newEvents) {
                      
                        $('#newevent').append(`<div onclick = "eventClic(${event.id},${event.AccountEvent_id}, '${event.state}', ${event.Account_id_guest})"id="${event.id}" class="newEvent" ><li><h3 class="newEvent">${event.name}<span >${event.date_init}</span></h3></li></div>`);
                    }

                })
            }
        }
    })

    /**  vérification si nouveau messages */
    //* requête vers l'api commune pour vérifier si il y a de nouveaux messages
    $.ajax({
        method: 'GET',
        url: `http://formenscop.bwb/message/checknew/${id}`,
        dataType: "JSON",
        success: function (result) {
            //* si il n'y a pas de nouveau messages, desactive le bouton message et enlève "le point rouge"
            if (result === false) {
                $('#button_message').attr("data-toggle", "");
                $('#message').attr('class', '');
                //* sinon il y a de nouveaux messages, requête vers l'api commune pour les récuperer au clic sur l'icone "message"
            } else {

                $('#button_message').click(function () {
                    $.ajax({
                        method: 'GET',
                        url: `http://formenscop.bwb/message/viewnew/${id}`,
                        dataType: "JSON",
                        success: function (result) {
                            //* pour chaque message non lu, on l'affiche dans la liste déroulante
                            // $('#newmsg').html('new msg');
                            for (msg of result) {
                                $('#newmsg').append(`<li><h3>${msg.subject}<span >${msg.date}</span></h3></li>`)

                            }
                        }
                    })
                })
            }

        },

    });


});
