//alert("updateHolidays");
$().ready(function () {
    $('[id^="button_validate_holiday"]').click(function () {
        var id = $(this).attr('value');
       //alert(id);
        // requête ajax en POST vers la page qui gère la connexion + data_form
                $.ajax({
            url: '/validate-request?edit=' + id,
            type: 'GET',
            success: function (code_html, statut) {
                $('#responseHoliday').html(
                    "<div class='alert alert-info'>Congé validé ! <img width='20' height='20' src='assets/images/loadeer.gif' alt='loader'></div>")

                setTimeout(
                    function () {
                        document.location.reload(true)
                    }, 2000);
            },
            error: function () {
                window.location.replace("/404");
            }
        })     
    })

    $('[id^="button_refuse_holiday"]').click(function () {
        var id = $(this).attr('value');
       //alert(id);
        // requête ajax en POST vers la page qui gère la connexion + data_form
                $.ajax({
            url: '/refuse-request?edit=' + id,
            type: 'GET',
            success: function () {
                $('#responseHoliday').html(
                    "<div class='alert alert-info'>Congé refusé ! <img width='20' height='20' src='assets/images/loadeer.gif' alt='loader'></div>")

                setTimeout(
                    function () {
                        document.location.reload(true)
                    }, 2000);
            },
            error: function () {
                window.location.replace("/404");
            }
        })     
    })
})