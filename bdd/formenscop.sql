-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mar 23 Juillet 2019 à 09:08
-- Version du serveur :  5.7.26-0ubuntu0.18.04.1
-- Version de PHP :  7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `formenscop`
--

-- --------------------------------------------------------

--
-- Structure de la table `Ability`
--

CREATE TABLE `Ability` (
  `id` int(11) NOT NULL,
  `Trainee_Account_id` int(11) NOT NULL,
  `Skill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Ability`
--

INSERT INTO `Ability` (`id`, `Trainee_Account_id`, `Skill_id`) VALUES
(1, 5, 2),
(2, 5, 3);

-- --------------------------------------------------------

--
-- Structure de la table `Accompagniment`
--

CREATE TABLE `Accompagniment` (
  `Trainee_Account_id` int(11) NOT NULL,
  `Coach_Salaried_Account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Accompagniment`
--

INSERT INTO `Accompagniment` (`Trainee_Account_id`, `Coach_Salaried_Account_id`) VALUES
(5, 6);

-- --------------------------------------------------------

--
-- Structure de la table `Account`
--

CREATE TABLE `Account` (
  `id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `FirstName` varchar(45) DEFAULT NULL,
  `Birthday` date DEFAULT NULL,
  `Email` varchar(45) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `activate` varchar(5) NOT NULL,
  `Address_id` int(11) NOT NULL,
  `Role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Account`
--

INSERT INTO `Account` (`id`, `Name`, `FirstName`, `Birthday`, `Email`, `Password`, `activate`, `Address_id`, `Role_id`) VALUES
(1, 'MacGillivray', 'Wilbur', '1975-08-17', 'wmacgillivray0@macromedia.com', '1234', 'true', 1, 1),
(2, 'Guarin', 'Hadrian', '1976-11-21', 'hguarin1@prlog.org', '1234', 'true', 2, 2),
(3, 'Birrel', 'Yasmin', '1974-09-30', 'ybirrel2@stanford.edu', '1234', 'true', 3, 2),
(4, 'Tyers', 'Astrid', '1986-04-03', 'atyers3@lulu.com', '1234', 'false', 4, 2),
(5, 'Palfery', 'Barbi', '1986-08-02', 'bpalfery4@dagondesign.com', '1234', 'true', 5, 3),
(6, 'McGuigan', 'Emmalynne', '1971-03-06', 'emcguigan5@g.co', '1234', 'true', 6, 4),
(7, 'Tippings', 'Paolo', '1973-09-11', 'ptippings6@bbb.org', '1234', 'true', 7, 5),
(8, 'Berdale', 'Margery', '1972-06-26', 'mberdale7@spiegel.de', '1234', 'true', 8, 5),
(9, 'Copeland', 'Oralle', '1984-08-06', 'ocopeland8@google.es', '1234', 'true', 9, 6),
(10, 'HeraultCorps', NULL, NULL, 'cyitzhakof9@storify.com', '1234', 'true', 10, 7),
(11, 'OncleBens Corps', NULL, NULL, 'bootstrap@example.com', '1234', 'true', 11, 7);

-- --------------------------------------------------------

--
-- Structure de la table `AccountEvent`
--

CREATE TABLE `AccountEvent` (
  `id` int(11) NOT NULL,
  `Event_id` int(11) NOT NULL,
  `Account_id_creator` int(11) NOT NULL,
  `Account_id_guest` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `AccountEvent`
--

INSERT INTO `AccountEvent` (`id`, `Event_id`, `Account_id_creator`, `Account_id_guest`) VALUES
(5, 1, 1, 2),
(6, 1, 1, 3),
(7, 2, 5, 5),
(8, 3, 7, 8);

-- --------------------------------------------------------

--
-- Structure de la table `Address`
--

CREATE TABLE `Address` (
  `id` int(11) NOT NULL,
  `zipcode` int(11) NOT NULL,
  `city` varchar(45) NOT NULL,
  `country` varchar(45) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `Type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Address`
--

INSERT INTO `Address` (`id`, `zipcode`, `city`, `country`, `number`, `name`, `Type_id`) VALUES
(1, 34567, 'Richeroy', 'France', 12, 'Alfred Goucci', 2),
(2, 66789, 'Arthurville', 'France', NULL, 'de la fournaise', 3),
(3, 11212, 'Narbin', 'France', 123, 'étroit', 5),
(4, 23789, 'Bizier', 'France', 1, 'Lucie Barthes', 3),
(5, 11000, 'Narbonne', 'France', 9, 'saint Seb', 5),
(6, 56999, 'Trou français', 'France', NULL, 'Pauline de la débauche', 4),
(7, 34000, 'Montpellier', 'France', 5678, 'Evan l\'audatieux', 2),
(8, 34567, 'Slip', 'France', 54, 'Pierre et Marie Cuirie', 2),
(9, 34370, 'Maureihan', 'France', 12, 'Pierre et Marie Curie', 1),
(10, 34500, 'Béziers', 'France', 1, 'Des Tulipes', 1),
(11, 99999, 'pezenas', 'France', 99, 'du slip', 1);

-- --------------------------------------------------------

--
-- Structure de la table `Administrator`
--

CREATE TABLE `Administrator` (
  `Salaried_Account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Administrator`
--

INSERT INTO `Administrator` (`Salaried_Account_id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Structure de la table `Advertisment`
--

CREATE TABLE `Advertisment` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `Company_id` int(11) NOT NULL,
  `Contract_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Advertisment`
--

INSERT INTO `Advertisment` (`id`, `date`, `title`, `description`, `Company_id`, `Contract_id`) VALUES
(1, '2019-07-10 12:15:40', 'Dev Front-End', 'oment was designed to work both in the browser and in Node.js.\r\n\r\nAll code should work in both of these environments, and all unit tests are run in both of these environments.\r\n\r\nCurrently the following browsers are used for the ci system: Chrome on Windows XP, IE 8, 9, and 10 on Windows 7, IE 11 on Windows 10, latest Firefox on Linux, and latest Safari on OSX 10.8 and 10.11.\r\n\r\nIf you want to try the sample codes below, just open your browser\'s console and enter them.\r\n\r\n', 1, 3),
(2, '2019-08-15 12:15:40', 'Dev JAVA', 'oment was designed to work both in the browser and in Node.js.\r\n\r\nAll code should work in both of these environments, and all unit tests are run in both of these environments.\r\n\r\nCurrently the following browsers are used for the ci system: Chrome on Windows XP, IE 8, 9, and 10 on Windows 7, IE 11 on Windows 10, latest Firefox on Linux, and latest Safari on OSX 10.8 and 10.11.\r\n\r\nIf you want to try the sample codes below, just open your browser\'s console and enter them.\r\n\r\n', 2, 1),
(3, '2019-08-01 12:15:40', 'BackEnd Symfony', 'oment was designed to work both in the browser and in Node.js.\r\n\r\nAll code should work in both of these environments, and all unit tests are run in both of these environments.\r\n\r\nCurrently the following browsers are used for the ci system: Chrome on Windows XP, IE 8, 9, and 10 on Windows 7, IE 11 on Windows 10, latest Firefox on Linux, and latest Safari on OSX 10.8 and 10.11.\r\n\r\nIf you want to try the sample codes below, just open your browser\'s console and enter them.\r\n\r\n', 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Advertissment_interest`
--

CREATE TABLE `Advertissment_interest` (
  `Trainee_Account_id` int(11) NOT NULL,
  `state` varchar(45) DEFAULT NULL,
  `Advertisment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Advertissment_interest`
--

INSERT INTO `Advertissment_interest` (`Trainee_Account_id`, `state`, `Advertisment_id`) VALUES
(5, 'like', 1);

-- --------------------------------------------------------

--
-- Structure de la table `Attendancy`
--

CREATE TABLE `Attendancy` (
  `Trainee_id` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Status` varchar(20) NOT NULL,
  `Comment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Attendancy`
--

INSERT INTO `Attendancy` (`Trainee_id`, `Date`, `Status`, `Comment_id`) VALUES
(2, '2019-07-17', 'present', 1),
(2, '2019-07-18', 'present', 3),
(2, '2019-07-19', 'absent', 2),
(2, '2019-07-20', 'present', 4),
(3, '2019-07-17', 'present', 6),
(3, '2019-07-19', 'absent', 5),
(4, '2019-07-17', 'absent', 8),
(4, '2019-07-18', 'present', 7),
(5, '2019-07-18', 'absent', 9),
(5, '2019-07-19', 'present', 10),
(2, '2019-07-17', 'present', 1),
(2, '2019-07-18', 'present', 3),
(2, '2019-07-19', 'absent', 2),
(2, '2019-07-20', 'present', 4),
(3, '2019-07-17', 'present', 6),
(3, '2019-07-19', 'absent', 5),
(4, '2019-07-17', 'absent', 8),
(4, '2019-07-18', 'present', 7),
(5, '2019-07-18', 'absent', 9),
(5, '2019-07-19', 'present', 10);

-- --------------------------------------------------------

--
-- Structure de la table `Coach`
--

CREATE TABLE `Coach` (
  `Salaried_Account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Coach`
--

INSERT INTO `Coach` (`Salaried_Account_id`) VALUES
(6);

-- --------------------------------------------------------

--
-- Structure de la table `Comment`
--

CREATE TABLE `Comment` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Comment`
--

INSERT INTO `Comment` (`id`, `comment`) VALUES
(1, 'Je regarde des photos de celles que j’ai aimées.\r\nVous ne les trouverez dans aucun album, je les vois, pourtant, dans la forêt sombre de ma mémoire.'),
(2, 'Comme elles se ressemblent !\r\nVisage ovale, très fin, regard très doux, sont-elles une ?\r\nEst-elle plusieurs ?'),
(3, 'Je regarde les photos de celles qui m’ont aimé.\r\nUn jour ou mille nuits ; une vie, ma vie avant, ma vie après, avant qui ? après qui ?\r\nToutes semblables, et toutes différentes, une et plusieurs, pour une vie.'),
(4, 'Je recherche dans mes cahiers des notes sur celles que j’ai aimées, qui m’ont aimé.\r\nPas forcément les mêmes ; pays à multiples provinces ; plaine et coteaux, brouillards et grands soleils. Une ou plusieurs ?'),
(5, 'Je retrouve dans mes valises des lettres manuscrites qui disent des bonheurs simples.\r\nDes malheurs simples.'),
(6, 'Visage ovale, regard très doux ; comme un parfum qui dure.'),
(7, 'Je suis là, maman\r\ncachée au fond de tes entrailles\r\nmes cellules vibrent\r\nton cœur bat fort\r\nje sens la chaleur de ta voix\r\nelle me caresse déjà'),
(8, 'Je suis un cerf volant\r\nemmailloté dans tes bras\r\nLe cerf volant du Paradis\r\nsans âge'),
(9, 'Les cloches sonnent'),
(10, 'Je suis là, maman\r\nj’arrive\r\nun bouquet parfumé dans mes pensées');

-- --------------------------------------------------------

--
-- Structure de la table `Communication`
--

CREATE TABLE `Communication` (
  `id` int(11) NOT NULL,
  `Account_id_sender` int(11) NOT NULL,
  `Account_id_receiver` int(11) NOT NULL,
  `Message_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Communication`
--

INSERT INTO `Communication` (`id`, `Account_id_sender`, `Account_id_receiver`, `Message_id`) VALUES
(1, 1, 2, 1),
(2, 1, 4, 1),
(3, 1, 5, 1),
(4, 1, 3, 1),
(7, 1, 1, 7),
(5, 3, 4, 4),
(6, 5, 1, 8),
(10, 7, 3, 1),
(9, 8, 9, 5),
(8, 9, 7, 3),
(11, 9, 5, 12),
(12, 9, 5, 14),
(13, 9, 5, 15),
(14, 9, 5, 16),
(15, 9, 5, 17),
(16, 9, 5, 18),
(17, 9, 5, 19),
(18, 9, 5, 20),
(19, 9, 5, 21),
(20, 9, 5, 22),
(21, 9, 5, 23),
(22, 9, 5, 24),
(23, 9, 5, 25),
(24, 9, 5, 26),
(25, 9, 5, 27),
(26, 9, 5, 28),
(27, 9, 5, 29),
(28, 9, 5, 30),
(29, 9, 5, 31),
(30, 9, 5, 32),
(31, 9, 5, 33),
(32, 9, 5, 34),
(33, 9, 5, 35),
(34, 9, 5, 36),
(35, 9, 5, 37),
(36, 9, 5, 38),
(37, 9, 5, 39),
(38, 9, 5, 40),
(39, 9, 5, 41),
(40, 9, 5, 42),
(41, 9, 5, 43),
(42, 9, 5, 44),
(43, 9, 5, 45),
(44, 9, 5, 46),
(45, 9, 5, 47),
(46, 9, 5, 48),
(47, 9, 5, 49),
(48, 9, 5, 50),
(49, 9, 5, 51),
(50, 9, 5, 52),
(51, 9, 5, 53),
(52, 9, 5, 54),
(53, 9, 5, 55),
(54, 9, 5, 56),
(55, 9, 5, 57),
(56, 9, 5, 58),
(57, 9, 5, 59),
(58, 9, 5, 60),
(59, 9, 5, 61),
(60, 9, 5, 62),
(61, 9, 5, 63),
(62, 9, 5, 64),
(63, 9, 5, 65);

-- --------------------------------------------------------

--
-- Structure de la table `Company`
--

CREATE TABLE `Company` (
  `id` int(11) NOT NULL,
  `Account_id` int(11) NOT NULL,
  `Coach_Salaried_Account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Company`
--

INSERT INTO `Company` (`id`, `Account_id`, `Coach_Salaried_Account_id`) VALUES
(1, 10, 6),
(2, 11, 6);

-- --------------------------------------------------------

--
-- Structure de la table `Contract`
--

CREATE TABLE `Contract` (
  `id` int(11) NOT NULL,
  `type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Contract`
--

INSERT INTO `Contract` (`id`, `type`) VALUES
(1, 'CDD'),
(2, 'CDI'),
(3, 'stage');

-- --------------------------------------------------------

--
-- Structure de la table `Coordinator`
--

CREATE TABLE `Coordinator` (
  `Salaried_Account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Coordinator`
--

INSERT INTO `Coordinator` (`Salaried_Account_id`) VALUES
(9);

-- --------------------------------------------------------

--
-- Structure de la table `Event`
--

CREATE TABLE `Event` (
  `id` int(11) NOT NULL,
  `date_init` date NOT NULL,
  `date_end` date NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  `EventType_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Event`
--

INSERT INTO `Event` (`id`, `date_init`, `date_end`, `name`, `description`, `EventType_id`) VALUES
(1, '2019-07-16', '2019-07-17', 'apero !!!', 'gnissim, egestas est. Aenean fermentum ipsum ultrices nulla varius, quis ultricies massa ultrices. Nullam id enim sed dolor mattis facilisis. Ut egestas finibus libero, sed ultricies lacus bibendum ut. Nullam at egestas nunc. Maecenas nisl ligula, tincidunt eu tempor id, sagittis eget ante. Nulla nisl lectus, molestie quis quam quis, bibendum porta nisl. Donec at libero maximus, tincidunt lacus ac, iaculis magna. Proin dictum matt', 1),
(2, '2019-07-19', '2019-07-20', 'stage !!', 'urna suscipit eget. Suspendisse augue lorem, facilisis in mattis vehicula, sagittis nec libero. Donec neque leo, auctor vel metus sit amet, tincidunt convallis nunc. Praesent molestie, urna sagittis eleifend pulvinar, orci urna posuere dui, a feugiat elit odio at sapien. Integer urna tellus, tempor ac magna nec, pharetra laoreet tellus. Integer sit amet dui libero. Curabitur id sollicitudin purus. Donec ac turpis ut elit ultricies lobortis. Nam malesuada ornare quam, a lobor', 1),
(3, '2019-07-29', '2019-08-01', 'Offre d\'emploi', 'Aenean fermentum ipsum ultrices nulla varius, quis ultricies massa ultrices. Nullam id enim sed dolor mattis facilisis. Ut egestas finibus libero, sed ultricies lacus bibendum ut. Nullam at egestas nunc. Maecenas nisl ligula, tincidunt eu tempor id, sagittis eget ante. Nulla nisl lectus, molestie quis quam quis, bibendum porta nisl. Donec at libero maximus, tincidunt lacus ac, iaculis magna. Proin dictum mattis leo, nec pellentesque magna sollicitudin nec. Sed vehicula efficitur sapien, faucibus tempus tortor vehicula vitae. Cras posuere sapien tempus cursus mattis.\r\n\r\n', 1),
(4, '2019-10-01', '2019-10-01', 'test calendar', 'test calendar create ...', 1),
(5, '2019-07-22', '2019-07-22', 'Test calendar !!', 'test !!!', 1),
(6, '2019-07-29', '2019-07-30', NULL, '					ligne table Event??							', 4),
(7, '2019-07-29', '2019-07-30', 'test', 'azertyuiop												', 4);

-- --------------------------------------------------------

--
-- Structure de la table `EventState`
--

CREATE TABLE `EventState` (
  `AccountEvent_id` int(11) NOT NULL,
  `state` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `EventState`
--

INSERT INTO `EventState` (`AccountEvent_id`, `state`) VALUES
(5, 'no'),
(6, 'maybe'),
(7, 'yes'),
(8, 'no');

-- --------------------------------------------------------

--
-- Structure de la table `EventType`
--

CREATE TABLE `EventType` (
  `id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `EventType`
--

INSERT INTO `EventType` (`id`, `type`) VALUES
(1, 'congés'),
(2, 'module'),
(3, 'rdv'),
(4, 'autre');

-- --------------------------------------------------------

--
-- Structure de la table `Expertise`
--

CREATE TABLE `Expertise` (
  `Advertisment_id` int(11) NOT NULL,
  `Advertisment_Company_id` int(11) NOT NULL,
  `Skill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Expertise`
--

INSERT INTO `Expertise` (`Advertisment_id`, `Advertisment_Company_id`, `Skill_id`) VALUES
(1, 10, 2),
(1, 10, 4);

-- --------------------------------------------------------

--
-- Structure de la table `Holiday`
--

CREATE TABLE `Holiday` (
  `id` int(11) NOT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `Salaried_Account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Holiday`
--

INSERT INTO `Holiday` (`id`, `start`, `end`, `status`, `Salaried_Account_id`) VALUES
(1, '2019-08-18', '2019-09-01', 'demande', 4),
(2, '2019-08-29', '2019-09-10', 'demande', 5),
(3, '2019-10-18', '2019-10-28', 'valid', 6);

-- --------------------------------------------------------

--
-- Structure de la table `Message`
--

CREATE TABLE `Message` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `subject` text,
  `texte` text NOT NULL,
  `removed` varchar(5) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Message`
--

INSERT INTO `Message` (`id`, `subject`, `texte`, `removed`, `date`) VALUES
(1, 'sed dolor. Fusce mi lorem,', 'dolor dolor, tempus non, lacinia at, iaculis quis,', 'true', '2019-07-16 16:32:36'),
(2, 'neque et nunc. Quisque ornare', 'massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec,', NULL, '2019-07-16 16:32:36'),
(3, 'ipsum porta elit, a feugiat', 'semper auctor. Mauris vel turpis. Aliquam adipiscing lobortis risus.', NULL, '2019-07-16 16:32:36'),
(4, 'velit. Sed malesuada augue ut', 'bibendum sed, est. Nunc laoreet lectus quis massa.', NULL, '2019-07-16 16:32:36'),
(5, 'Fusce mi lorem, vehicula et,', 'facilisis lorem tristique aliquet. Phasellus fermentum convallis', NULL, '2019-07-16 16:32:36'),
(6, 'dolor, tempus non, lacinia at,', 'et malesuada fames ac turpis egestas. Fusce aliquet', 'true', '2019-07-16 16:32:36'),
(7, 'Sed pharetra, felis eget varius', 'lectus ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing', NULL, '2019-07-16 16:32:36'),
(8, 'sed, est. Nunc laoreet lectus', 'natoque penatibus et magnis dis parturient montes,', NULL, '2019-07-16 16:32:36'),
(9, 'Ut sagittis lobortis mauris. Suspendisse', 'Ut tincidunt vehicula risus. Nulla', 'true', '2019-07-16 16:32:36'),
(10, 'feugiat placerat velit. Quisque varius.', 'Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus', NULL, '2019-07-16 16:32:36'),
(11, 'sujet du message', 'contenu du message', NULL, '2019-07-16 16:32:36'),
(12, 'sujet 2', 'message2', NULL, '2019-07-16 16:32:36'),
(13, 'Sujet3', 'Message3', NULL, '2019-07-16 16:32:36'),
(14, 'sujet4', 'message4', NULL, '2019-07-16 16:32:36'),
(15, 'Sujet 5', 'Message 5', NULL, '2019-07-16 16:32:36'),
(16, 'Sujet 6', 'Message 6', NULL, '2019-07-16 16:32:36'),
(17, 'sujet 6', 'message 6', NULL, '2019-07-16 16:32:36'),
(18, 'sujet 6', 'message 6', NULL, '2019-07-16 16:32:36'),
(22, 'sujet 8', 'sujet 8', NULL, '2019-07-16 16:32:36'),
(23, 'sujet 8', 'sujet 8', NULL, '2019-07-16 16:32:36'),
(24, 'sujet 10', 'MEssage 10', NULL, '2019-07-16 16:32:36'),
(25, 'sujet 11', 'message 11', NULL, '2019-07-16 16:32:36'),
(26, 'sujet 12', 'message 12', NULL, '2019-07-16 16:32:36'),
(27, 'sujet 12', 'message 12', NULL, '2019-07-16 16:32:36'),
(28, 'sujet 12', 'message 12', NULL, '2019-07-16 16:32:36'),
(63, '', '', NULL, '2019-07-16 16:32:36'),
(64, 'Sujet 40', 'message 40', NULL, '2019-07-16 16:32:36'),
(65, 'popolaa', 'popopalalala', NULL, '2019-07-16 16:33:08');

-- --------------------------------------------------------

--
-- Structure de la table `MessageState`
--

CREATE TABLE `MessageState` (
  `Communication_id` int(11) NOT NULL,
  `readed` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `MessageState`
--

INSERT INTO `MessageState` (`Communication_id`, `readed`) VALUES
(1, 'true'),
(2, 'true'),
(3, NULL),
(4, NULL),
(5, NULL),
(6, NULL),
(7, 'true'),
(8, 'true'),
(9, NULL),
(10, 'true'),
(13, 'NULL'),
(14, 'NULL'),
(15, NULL),
(16, NULL),
(17, NULL),
(18, ''),
(19, ''),
(20, ''),
(21, ''),
(22, ''),
(23, ''),
(24, ''),
(25, ''),
(26, ''),
(27, ''),
(28, ''),
(29, ''),
(30, ''),
(31, ''),
(32, ''),
(33, ''),
(34, ''),
(35, ''),
(36, ''),
(37, ''),
(38, ''),
(39, ''),
(40, ''),
(41, ''),
(42, ''),
(43, ''),
(44, ''),
(45, ''),
(46, ''),
(47, ''),
(48, ''),
(49, ''),
(50, ''),
(51, ''),
(52, ''),
(53, ''),
(54, ''),
(55, ''),
(56, ''),
(57, ''),
(58, ''),
(59, ''),
(60, ''),
(61, ''),
(62, ''),
(63, '');

-- --------------------------------------------------------

--
-- Structure de la table `Module`
--

CREATE TABLE `Module` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `desciption` text,
  `start` date DEFAULT NULL,
  `volume` int(11) DEFAULT NULL,
  `Training_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Module`
--

INSERT INTO `Module` (`id`, `name`, `desciption`, `start`, `volume`, `Training_id`) VALUES
(1, 'HTML', 'Initiation au HTML', NULL, 25, 1),
(2, 'CSS', 'Initiation au CSS', NULL, 25, 1),
(3, 'JAVASCRIPT', 'Initiation au Javascript', NULL, 25, 1);

-- --------------------------------------------------------

--
-- Structure de la table `Role`
--

CREATE TABLE `Role` (
  `id` int(11) NOT NULL,
  `designiation` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Role`
--

INSERT INTO `Role` (`id`, `designiation`, `description`) VALUES
(1, 'RH', 'salarié responsable des ressources humaines'),
(2, 'formation', 'stagiaire en formation'),
(3, 'accompagnement', 'Stagiaire en accompagnement'),
(4, 'accompagnateur', 'salarié responsable de l\'accompagnement de stagiaires'),
(5, 'formateur', 'salarié en charge de la formation des stagiaires'),
(6, 'coordinateur', 'salarié en charge de la coordination salarié/entreprises'),
(7, 'entreprise', 'Entreprise partenaire');

-- --------------------------------------------------------

--
-- Structure de la table `Salaried`
--

CREATE TABLE `Salaried` (
  `Account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Salaried`
--

INSERT INTO `Salaried` (`Account_id`) VALUES
(1),
(6),
(7),
(8),
(9);

-- --------------------------------------------------------

--
-- Structure de la table `Skill`
--

CREATE TABLE `Skill` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Skill`
--

INSERT INTO `Skill` (`id`, `name`, `description`) VALUES
(1, 'Global Custody', 'est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi'),
(2, 'WiMAX', 'imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in'),
(3, 'Gift Tax', 'vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget'),
(4, 'FCL', 'in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis'),
(5, 'Stand-up Comedy', 'sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed'),
(6, 'TTS', 'vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo'),
(7, 'Product Management', 'amet consectetuer adipiscing elit proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante'),
(8, 'Investments', 'accumsan odio curabitur convallis duis consequat dui nec nisi volutpat'),
(9, 'Real Estate Due Diligence', 'nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus'),
(10, 'Business Ideas', 'pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla'),
(11, 'Epigenetics', 'nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec'),
(12, 'Flight Training', 'pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique'),
(13, 'Heavy Equipment', 'curae nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a'),
(14, 'zLinux', 'in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus'),
(15, 'Crystal Reports', 'enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula'),
(16, 'PWS', 'consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus'),
(17, 'Sustainable Agriculture', 'aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros'),
(18, 'FPC 1', 'proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem'),
(19, '21 CFR', 'lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl'),
(20, 'Adobe Acrobat', 'volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec');

-- --------------------------------------------------------

--
-- Structure de la table `Student`
--

CREATE TABLE `Student` (
  `Trainee_Account_id` int(11) NOT NULL,
  `Training_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Student`
--

INSERT INTO `Student` (`Trainee_Account_id`, `Training_id`) VALUES
(2, 1),
(4, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Trainee`
--

CREATE TABLE `Trainee` (
  `Account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Trainee`
--

INSERT INTO `Trainee` (`Account_id`) VALUES
(2),
(3),
(4),
(5);

-- --------------------------------------------------------

--
-- Structure de la table `Trainer`
--

CREATE TABLE `Trainer` (
  `Salaried_Account_id` int(11) NOT NULL,
  `Coordinator_Salaried_Account_id` int(11) NOT NULL,
  `Training_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Trainer`
--

INSERT INTO `Trainer` (`Salaried_Account_id`, `Coordinator_Salaried_Account_id`, `Training_id`) VALUES
(7, 9, 1),
(8, 9, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Training`
--

CREATE TABLE `Training` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `volume` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Training`
--

INSERT INTO `Training` (`id`, `name`, `start`, `end`, `volume`) VALUES
(1, 'Développeur web', NULL, NULL, 90),
(2, 'Paysagiste', NULL, NULL, 60);

-- --------------------------------------------------------

--
-- Structure de la table `Type`
--

CREATE TABLE `Type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Type`
--

INSERT INTO `Type` (`id`, `name`) VALUES
(1, 'rue'),
(2, 'avenue'),
(3, 'impasse'),
(4, 'lieux-dit'),
(5, 'place');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Ability`
--
ALTER TABLE `Ability`
  ADD PRIMARY KEY (`id`,`Trainee_Account_id`,`Skill_id`),
  ADD KEY `fk_Ability_Trainee1_idx` (`Trainee_Account_id`),
  ADD KEY `fk_Ability_Skill1_idx` (`Skill_id`);

--
-- Index pour la table `Accompagniment`
--
ALTER TABLE `Accompagniment`
  ADD PRIMARY KEY (`Trainee_Account_id`,`Coach_Salaried_Account_id`),
  ADD KEY `fk_Accompagniment_Coach1_idx` (`Coach_Salaried_Account_id`);

--
-- Index pour la table `Account`
--
ALTER TABLE `Account`
  ADD PRIMARY KEY (`id`,`Address_id`,`Role_id`),
  ADD KEY `fk_Account_Address1_idx` (`Address_id`),
  ADD KEY `fk_Account_Role1_idx` (`Role_id`);

--
-- Index pour la table `AccountEvent`
--
ALTER TABLE `AccountEvent`
  ADD PRIMARY KEY (`id`,`Event_id`,`Account_id_creator`,`Account_id_guest`),
  ADD KEY `fk_AccountEvent_Event1_idx` (`Event_id`),
  ADD KEY `fk_AccountEvent_Account1_idx` (`Account_id_creator`),
  ADD KEY `fk_AccountEvent_Account2_idx` (`Account_id_guest`);

--
-- Index pour la table `Address`
--
ALTER TABLE `Address`
  ADD PRIMARY KEY (`id`,`Type_id`),
  ADD KEY `fk_Address_Type_idx` (`Type_id`);

--
-- Index pour la table `Administrator`
--
ALTER TABLE `Administrator`
  ADD PRIMARY KEY (`Salaried_Account_id`);

--
-- Index pour la table `Advertisment`
--
ALTER TABLE `Advertisment`
  ADD PRIMARY KEY (`id`,`Company_id`,`Contract_id`),
  ADD KEY `fk_Advertisment_Company1_idx` (`Company_id`),
  ADD KEY `fk_Advertisment_Contract1_idx` (`Contract_id`);

--
-- Index pour la table `Advertissment_interest`
--
ALTER TABLE `Advertissment_interest`
  ADD PRIMARY KEY (`Trainee_Account_id`,`Advertisment_id`),
  ADD KEY `fk_Advertissment_interest_Advertisment1_idx` (`Advertisment_id`);

--
-- Index pour la table `Coach`
--
ALTER TABLE `Coach`
  ADD PRIMARY KEY (`Salaried_Account_id`);

--
-- Index pour la table `Comment`
--
ALTER TABLE `Comment`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Communication`
--
ALTER TABLE `Communication`
  ADD PRIMARY KEY (`id`,`Account_id_sender`,`Account_id_receiver`,`Message_id`),
  ADD KEY `fk_Communication_Account1_idx` (`Account_id_sender`),
  ADD KEY `fk_Communication_Account2_idx` (`Account_id_receiver`),
  ADD KEY `fk_Communication_Message1_idx` (`Message_id`);

--
-- Index pour la table `Company`
--
ALTER TABLE `Company`
  ADD PRIMARY KEY (`id`,`Account_id`,`Coach_Salaried_Account_id`),
  ADD KEY `fk_Company_Account1_idx` (`Account_id`),
  ADD KEY `fk_Company_Coach1_idx` (`Coach_Salaried_Account_id`);

--
-- Index pour la table `Contract`
--
ALTER TABLE `Contract`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Coordinator`
--
ALTER TABLE `Coordinator`
  ADD PRIMARY KEY (`Salaried_Account_id`);

--
-- Index pour la table `Event`
--
ALTER TABLE `Event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `EventType_id` (`EventType_id`);

--
-- Index pour la table `EventState`
--
ALTER TABLE `EventState`
  ADD PRIMARY KEY (`AccountEvent_id`);

--
-- Index pour la table `EventType`
--
ALTER TABLE `EventType`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Expertise`
--
ALTER TABLE `Expertise`
  ADD PRIMARY KEY (`Advertisment_id`,`Advertisment_Company_id`,`Skill_id`),
  ADD KEY `fk_Expertise_Skill1_idx` (`Skill_id`);

--
-- Index pour la table `Holiday`
--
ALTER TABLE `Holiday`
  ADD PRIMARY KEY (`id`,`Salaried_Account_id`),
  ADD KEY `fk_Leave_Salaried1_idx` (`Salaried_Account_id`);

--
-- Index pour la table `Message`
--
ALTER TABLE `Message`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `MessageState`
--
ALTER TABLE `MessageState`
  ADD PRIMARY KEY (`Communication_id`);

--
-- Index pour la table `Module`
--
ALTER TABLE `Module`
  ADD PRIMARY KEY (`id`,`Training_id`),
  ADD KEY `fk_Module_Training1_idx` (`Training_id`);

--
-- Index pour la table `Role`
--
ALTER TABLE `Role`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Salaried`
--
ALTER TABLE `Salaried`
  ADD PRIMARY KEY (`Account_id`);

--
-- Index pour la table `Skill`
--
ALTER TABLE `Skill`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Student`
--
ALTER TABLE `Student`
  ADD PRIMARY KEY (`Trainee_Account_id`,`Training_id`),
  ADD KEY `fk_Student_Training1_idx` (`Training_id`);

--
-- Index pour la table `Trainee`
--
ALTER TABLE `Trainee`
  ADD PRIMARY KEY (`Account_id`);

--
-- Index pour la table `Trainer`
--
ALTER TABLE `Trainer`
  ADD PRIMARY KEY (`Salaried_Account_id`,`Coordinator_Salaried_Account_id`,`Training_id`),
  ADD KEY `fk_Trainer_Coordinator1_idx` (`Coordinator_Salaried_Account_id`),
  ADD KEY `fk_Trainer_Formation1_idx` (`Training_id`);

--
-- Index pour la table `Training`
--
ALTER TABLE `Training`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Type`
--
ALTER TABLE `Type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Ability`
--
ALTER TABLE `Ability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `Account`
--
ALTER TABLE `Account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `AccountEvent`
--
ALTER TABLE `AccountEvent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `Address`
--
ALTER TABLE `Address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `Advertisment`
--
ALTER TABLE `Advertisment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `Comment`
--
ALTER TABLE `Comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `Communication`
--
ALTER TABLE `Communication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT pour la table `Company`
--
ALTER TABLE `Company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `Contract`
--
ALTER TABLE `Contract`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `Event`
--
ALTER TABLE `Event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `EventType`
--
ALTER TABLE `EventType`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `Holiday`
--
ALTER TABLE `Holiday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `Message`
--
ALTER TABLE `Message`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT pour la table `Module`
--
ALTER TABLE `Module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `Role`
--
ALTER TABLE `Role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `Skill`
--
ALTER TABLE `Skill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `Training`
--
ALTER TABLE `Training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `Type`
--
ALTER TABLE `Type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Event`
--
ALTER TABLE `Event`
  ADD CONSTRAINT `Event_ibfk_1` FOREIGN KEY (`EventType_id`) REFERENCES `EventType` (`id`);

--
-- Contraintes pour la table `EventState`
--
ALTER TABLE `EventState`
  ADD CONSTRAINT `EventState_ibfk_1` FOREIGN KEY (`AccountEvent_id`) REFERENCES `AccountEvent` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
