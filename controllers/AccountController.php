<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\dao\DAORole;
use BWB\Framework\mvc\dao\DAOType;
use BWB\Framework\mvc\dao\DAOTraining;

use BWB\Framework\mvc\Controller;

class AccountController extends Controller
{

    protected $_userPost;
    protected $_id;
    protected $_user;
    protected $_userId;
    protected $_name;
    protected $_firstname;
    protected $_email;
    protected $_password;
    protected $_roleId;

  
    /**
     * Envois sur la page login
     *
     * @return void
     */
    public function getLogin()
    {
        $this->render('login');
    }


    /**
     * Deconnection utilisateur et redirige vers l'index'
     * @return void
     */
    public function getLogout()
    {
        $this->securityLoader();
        $this->security->deactivate();
        header('location: /');
    }


    /**
     * renvois a la page error 404 sur "/404"
     *
     * @return void
     */
    public function getError()
    {
        $this->render('404');
    }


    /**
     * Method pour recurperer tous les champs de account et des tables associées 
     * de tous les accounts
     *
     * @return void
     */
    public function getAll()
    {
        $accountInfo = (new DAOAccount)->getAll();
        //var_dump($accountInfo);
    }

    
    /**
     * Method pour recurperer tous les champs d'une id
     * de la table account et des tables associées
     *
     * @return void
     */
    public function retrieveById()
    {
        $id = $this->inputGet()['id'];
        $datasAccount = (new DAOAccount())->retrieve($id);
        $datasRole = (new DAORole())->getAll();
        $datasType = (new DAOType())->getAll();
        $datas = array($datasRole, $datasType, $datasAccount);

        $this->render('editAccount', $datas);
    }


    /**
     * Récupère toutes les données des tables par id et les renvoie vers la page détails des accounts 
     *
     * @return void
     */
    public function getAllById()
    {
        $id = $this->inputGet()['id'];
        $datasAccount = (new DAOAccount())->retrieve($id);
        $datasRole = (new DAORole())->getAll();
        $datasType = (new DAOType())->getAll();
        $datas = array($datasRole, $datasType, $datasAccount);

        $this->render('detailsAccount', $datas);
    }


    /**
     * Retourne le formulaire de création d'account ainsi que la liste des roles, types, training et caochs
     *
     * @return void 
     */
    public function createAccount()
    {
        $datasRole = (new DAORole())->getAll();
        $datasType = (new DAOType())->getAll();
        $datasTraining = (new DAOTraining())->getAll();
        $datasCoach = (new DAOAccount())->getAllByRole('4');

        $datas = array($datasRole, $datasType, $datasTraining, $datasCoach);

        $this->render('createAccount', $datas);
    }


    /**
     * Méthode qui permet de creer un account
     *
     * @return void
     */
    public function create()
    {
        // Recuperation des valeurs postées
        $datas = $this->inputPost();


        // Crée un nouvel objet DAOAccount et execute sa methode create()
        switch ($datas['Role_id']) {
            case '1':
                echo ((new DAOAccount)->createAdministrator($datas)) ?  'true' : 'false';
                break;
            case '2':
                echo ((new DAOAccount)->createStudent($datas)) ?  'true' : 'false';
                break;
            case '3':
                echo ((new DAOAccount)->createAccompagniment($datas)) ?  'true' : 'false';
                break;
            case '4':
                echo ((new DAOAccount)->createCoach($datas)) ?  'true' : 'false';
                break;
            case '5':
                echo ((new DAOAccount)->create($datas)) ?  'true' : 'false';
                break;
            case '6':
                echo ((new DAOAccount)->createCoordinator($datas)) ?  'true' : 'false';
                break;
            case '7':
                echo ((new DAOAccount)->createCompany($datas)) ?  'true' : 'false';
                break;
            default:
                echo ((new DAOAccount)->create($datas)) ?  'true' : 'false';
                break;
        }
    }


    /**
     * Méthode qui permet d'éditer un account sélectionné
     *
     * @return void
     */
    public function updateById()
    {
        $datas = $this->inputPut();
        return (new DAOAccount)->update($datas);
    }


    /**
     * Méthode qui compare l'email et le password avec la base de donnée, si ok -> creation du token avec les infos email, password et role
     *
     * @return void
     */
    public function getVerifLogin()
    {
        $this->_userPost = $this->inputPost();

        $_userConnexion = new DAOAccount;
        $this->_user = $_userConnexion->getLogin($this->_userPost['email'], $this->_userPost['password']);

        if (is_object($this->_user)) {

            $this->securityLoader();
            $this->security->generateToken($this->_user);

            $this->_roleId = $this->_user->getRoles();

            $this->_id = $this->_user->getId();
            $this->_name =  $this->_user->getName();;
            $this->_firstname =  $this->_user->getFirstname();;
            $this->_email =  $this->_user->getEmail();;

            $_SESSION['user_id'] = $this->_id;
            $_SESSION['name'] = $this->_name;
            $_SESSION['firstname'] = $this->_firstname;
            $_SESSION['email'] = $this->_email;

            echo 'true';
        } else {
            echo 'false';
        }
    }


    /**
     * Méthode qui si le token existe récupère le role_id dans le token et redirige vers l'application concernée
     *
     * @return void
     */
    public function userHome()
    {
        $tkn = $this->security->verifyToken($_COOKIE['tkn']);

        if (!empty($_COOKIE['tkn'])) {

            /**
             * Mettre le "$this->render('index');" sur le bon "cas" et
             * les "header location" dans les autres.
             * 
             * Par exemple si la connection est de GestionRH
             * il faut mettre le "render" dans le cas '1' et
             * mettre les "header location" dans les autres pour rediriger en fonction du role
             * 
             * $this->render('index');
             * 
             * header('Location: http://entreprise.formenscop.bwb');
             * header('Location: http://salarie.formenscop.bwb');
             * header('Location: http://gestionrh.formenscop.bwb');
             * header('Location: http://stagiaire.formenscop.bwb');
             * 
             */

            switch ($tkn->roles) {
                case '1':
                    // redirection vers l'application RH';
                    $this->render('index');
                    break;
                case '2':
                case '3':
                    // redirection vers l'application stagiaire
                    header('Location: http://stagiaire.formenscop.bwb');
                    break;
                case '4':
                case '5':
                case '6':
                    // redirection vers l'application Salarié';
                    header('Location: http://salarie.formenscop.bwb');
                    break;
                case '7':
                    // redirection vers l'application entreprise partenaire';
                    header('Location: http://entreprise.formenscop.bwb');
                    break;
            }
        } else {

            $this->getError();
        }
    }
}
