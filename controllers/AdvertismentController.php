<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\dao\DAOAdvertisment;
use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\dao\DAOContract;
use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\dao\DAOCompany;

class AdvertismentController extends Controller
{

    protected $id;
    protected $date;
    protected $title;
    protected $description;
    protected $Company_id;
    protected $Contract_id;


    /**
     * Méthode qui affiche la liste des offres d'emploi
     *
     * @return void
     */
    public function getAdvertisments()
    {
        /** on récupère les données dans chaque tables concernées */
        $datasAd = (new DAOAdvertisment())->getAll();
        $datasComp = (new DAOCompany())->getAll();
        $datasAc = (new DAOAccount())->getAll();
        $datasCo = (new DAOContract())->getAll();

        /** on crée un tableau pour récupérer les données des 4 tables */
        $datas = array($datasAd, $datasComp, $datasAc, $datasCo);

        $this->render('listeAdvertisments', $datas);
    }

    /**
     * Retourne le formulaire qui permet de modifier une offre d'emploi sélectionnée par id
     *
     * @param  mixed $id
     *
     * @return void
     */
    public function getAdvertisment($id)
    {
        $datas = (new DAOAdvertisment())->retrieve($id);
        $this->render('editAdvertisment', $datas);
    }

    
    /**
     * Méthode qui permet de modifier une offre d'emploi sélectionnée par id
     *
     * @return void
     */
    public function updateAdvertisment()
    {
        $datas = $this->inputPut();

        return (new DAOAdvertisment())->update($datas);
    }

   
    public function createAdvertisment()
    {
      
    }
}
