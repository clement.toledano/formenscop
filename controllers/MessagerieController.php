<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\controllers\DefaultController;
use BWB\Framework\mvc\dao\DAOMessagerie;
use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\models\Account;




class MessagerieController extends DefaultController
{
    /**
     * Retourne la vue message
     * @link / methode invoquée lors d'une requete vers les messages
     */
    public function getMessagerie()
    {
       
        $tkn = $this->security->verifyToken($_COOKIE['tkn']);

        $userRow = ( new DAOAccount())->getFullUser($tkn->username);
        $listUsers = ( new DAOMessagerie)->listeUsers();
        $senderId = $userRow->getId();
        
        $array = array("senderId"=>$senderId,"listUsers"=>$listUsers);
        $this->render("chat",$array);  
    }

   /**
     * Retourne la vue message
     * @link / methode invoquée lors d'une requete vers les messages
     */
    public function getArchivesMessagerie()
    {
        $data = $this->inputPost(); // Recuperation des valeurs postées
        $daoMessagerie = new DAOMessagerie();
        $archives =  $daoMessagerie->getAllBy($data);

        $archives = json_encode($archives); // Encode le tableau en json pour pouvoir être exploité en JS
        print_r($archives);
       
    }


    /**
     * Envoi d'un message
     */
    public function sendMessagerie()
    {   
      
        $data = $this->inputPost(); // Recuperation des valeurs postées
        $testMessage = new DAOMessagerie(); // Crée un nouvel objet DAOMessagerie 
        $allReceivers = $data["id_receivers"];
        // La boucle doit se faire ICI =================================================
        
            $testMessage->create($data); // Execute la methode create() de DAOMessagerie
    
        // et se terminer là ==========================================================
    }


    /**
     * Envoi d'un message
     */
    public function suppMessage()
    {   
        $data = $this->inputPost(); // Recuperation des valeurs postées
        $myDaoMsg = new DAOMessagerie(); // Crée un nouvel objet DAOMessagerie 
        $myDaoMsg->eraseMsg($data); // Execute la methode eraseMsg() de DAOMessagerie
    }

    
}
