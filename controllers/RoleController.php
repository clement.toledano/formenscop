<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\dao\DAORole;
use BWB\Framework\mvc\Controller;

class RoleController extends Controller
{

    /**
     * Retourne la vue de la liste des roles
     * Méthode qui Récupère toutes les données de la table Role
     *
     * @return void
     */
    public function getAll()
    {
        $datas = (new DAORole())->getAll();
        $this->render("listeRole", $datas);
    }

    /**
     * getAllRoleUsed
     *
     * @return void
     */
    public function getAllRoleUsed()
    {
        $datas = (new DAORole())->getAllRoleUsed();
        $this->render("listeRole", $datas);
    }

    /**
     * Retourne le formulaire de création de role
     *
     * @return void
     */
    public function createRole()
    {
        $this->render("createRole");
    }

    /**
     *  Méthode qui crée un role ou le delete (cache) si il possède un id
     *
     * @return void
     */
    public function createEndDelete()
    {
        $array = $this->inputPost();

        if (isset($array['id'])) {
            echo ((new DAORole)->delete($array['id'])) ?  'true' : 'false';
        } else {
            echo ((new DAORole())->create($array)) ?  'true' : 'false';
        }
    }

    /**
     * Retourne la vue du formulaire de modificaiton d'un role
     * Méthode qui récupère les données de la table Role 
     *
     * @return void
     */
    public function retrieveById()
    {
        $id = $this->inputGet()['id'];

        $datas = (new DAORole())->retrieve($id);
        $this->render('editRole', $datas);
    }

    /**
     * Méthode invoquée pour modifier les données de la table Role 
     *
     * @return void
     */
    public function updateById()
    {
        $datas = $this->inputPut();
        echo ((new DAORole())->update($datas)) ?  'true' : 'false';
    }

    public function update(){}
}
