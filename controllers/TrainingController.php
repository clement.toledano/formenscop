<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\dao\DAOTraining;
use BWB\Framework\mvc\dao\DAOTrainer;
use BWB\Framework\mvc\dao\DAOAccount;

class TrainingController extends Controller
{
    
    /**  */ 
    /**
     * Retourne la vue de la liste des trainings
     *
     * @return void
     */
    public function getTrainings()
    {
        /** récupère les données dans la table training */
        $datasTraining = (new DAOTraining())->getAll();
        //récupère les données dans la table account
        $datasAccount = (new DAOAccount())->getAll();

        /** on crée un tableau pour pouvoir récupérer toutes les données */ 
        $datas = array($datasTraining, $datasAccount);

        /** on affiche la liste des formations avec les données du tableau */
        $this->render('listeTrainings', $datas);
    }

    /**
     * Retourne le formulaire de création de training et affiche la liste des trainers et coordinators
     * Méthode invoquée pour créer un training 
     *
     * @return void
     */
    public function createTraining()
    {
        $datasFormateur = (new DAOAccount())->getAllByRole("5");
        $datasCoordinateur = (new DAOAccount())->getAllByRole("6");

        $datas = array($datasFormateur, $datasCoordinateur);
        $this->render("createTraining", $datas);
    }

    /**
     * Méthode invoquée pour créer une formation
     *
     * @return void
     */
    public function create()
    {
        $array = $this->inputPost();
        echo ((new DAOTraining)->create($array)) ?  'true' : 'false';
    }

    /**
     * Retourne la vue du formulaire de modification d'un training par id 
     * Méthode qui récupère et affiche toutes les données du training sélectionné
     *
     * @return void
     */
    public function getEditTraining()
    {
        $id = $this->inputGet()['id'];
        $datasTraining = (new DAOTraining())->retrieve($id);
        $datasFormateur = (new DAOAccount())->getAllByRole("5");
        $datasCoordinateur = (new DAOAccount())->getAllByRole("6");
        $datasTrainer = (new DAOTrainer())->getAll();
        $datasAccount = (new DAOAccount())->getAll();

        $datas = array($datasTraining, $datasFormateur, $datasCoordinateur, $datasTrainer, $datasAccount);

        $this->render("editTraining", $datas);
    }

    /**
     * Méthode invoquée pour enregistrer les modifications d'un training 
     *
     * @return void
     */
    public function updateById()
    {
        $datas = $this->inputPut();
        echo ((new DAOTraining)->update($datas)) ?  'true' : 'false';
    }
}
