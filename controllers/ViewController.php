<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;

use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\dao\DAOHoliday;
use BWB\Framework\mvc\dao\DAOModule;
use BWB\Framework\mvc\dao\DAORole;

class ViewController extends Controller
{

    /**
     * Retourne la page d'accueil 
     *
     * @return void
     */
    public function home()
    {
        $this->render("index");
    }

    /**
     * Retourne la page de login
     *
     * @return void
     */
    public function login()
    {
        $this->render("login");
    }

    /**
     * Retourne la page du calendrier
     *
     * @return void
     */
    public function calendar()
    {
        $this->render("calendar");
    }
 
    /**
     * Méthode invoquée pour récupérer et afficher la liste des accounts selon leur role
     *
     * @return void
     */
    public function getAllByRole()
    {
        $getRole = $this->inputGet()['role'];
        $roles = (new DAORole())->getAll();
        foreach ($roles as $role) {
            if ($getRole === $role->getDesigniation()){
                $datas = array((new DAOAccount())->getAllByRole($role->getId()), $getRole);
                
                $this->render('listeAccount', $datas);
            }
        }
    }

    /**
     * Retourne la vue de la liste demandes de congés
     * Méthode invoquée pour récupérer toutes les demandes de congés
     *
     * @return void
     */
    public function getHolidayRequests()
    {
        $datas = (new DAOHoliday())->getHolidayRequests();
        $this->render('listeHolidayRequests', $datas);
    }

    /**
     * Retourne la liste de congés validés
     * Méthode invoquée pour récupérer toutes les congés validés
     *
     * @return void
     */
    public function getHolidays()
    {
        $datas = (new DAOHoliday())->getHolidays();
        $this->render("listeHolidays", $datas);
    }

    /**
     * Méthode invoquée pour récupérer et retourner la liste des modules
     *
     * @return void
     */
    public function getModules()
    {
        $datas = (new DAOModule())->getAll();
        $this->render('listeModules', $datas);
    }
   
}
