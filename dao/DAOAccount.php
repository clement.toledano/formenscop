<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use BWB\Framework\mvc\models\Account;
use PDO;


class DAOAccount extends DAO
{

    /**
     * Retourne toutes les infos par le l'id du role
     *
     * @param  mixed $role_id
     *
     * @return array
     */
    public function getAllByRole($role_id)
    {
        $query = "SELECT ac.id, ac.Name, ac.FirstName, ac.Birthday, ac.Email, ac.Password,ac.Address_id,ac.Role_id, ac.activate, ro.designiation, ro.description, ad.zipcode, ad.city, ad.country, ad.number, ad.name, ty.name FROM Account as ac JOIN Role as ro ON ac.Role_id = ro.id JOIN Address as ad ON ac.Address_id = ad.id JOIN Type as ty ON ad.Type_id = ty.id Where ac.Role_id=" . $role_id;
        $result = $this->getPdo()->query($query);
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Account');
        return $result->fetchAll();
    }



    /**
     * retourne toute la ligne
     *
     * @return array
     */
    public function getAll()
    {


        $query = "SELECT ac.id, ac.Name, ac.FirstName, ac.Birthday, ac.Email, ac.Password,ac.Address_id,ac.Role_id, ac.activate, ro.designiation, ro.description, ad.zipcode, ad.city, ad.country, ad.number, ad.name, ty.name FROM Account as ac JOIN Role as ro ON ac.Role_id = ro.id JOIN Address as ad ON ac.Address_id = ad.id JOIN Type as ty ON ad.Type_id = ty.id";
        $result = $this->getPdo()->query($query);
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Account');
        //var_dump($result);
        return $result->fetchAll();
    }

    /**
     * afficher les infos d'un account par son id
     *
     * @param  mixed $id
     *
     * @return array
     */
    public function retrieve($id)
    {
        $query = "SELECT ac.id, ac.Name, ac.FirstName, ac.Birthday, ac.Email, ac.Password,ac.Address_id,ac.Role_id, ac.activate, ro.designiation, ro.description, ad.zipcode, ad.city, ad.country, ad.number, ad.name, ad.Type_id FROM Account as ac JOIN Role as ro ON ac.Role_id = ro.id JOIN Address as ad ON ac.Address_id = ad.id JOIN Type as ty ON ad.Type_id = ty.id WHERE ac.id=" . $id;

        $result = $this->getPdo()->query($query);
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Account');

        $query1 = "SELECT ad.zipcode, ad.city, ad.country, ad.number, ad.name, ad.Type_id FROM Address as ad JOIN Account as ac ON ac.Address_id = ad.id WHERE ac.id=" . $id;

        $address = $this->getPdo()->query($query1);
        $address->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Address');

        $datas = array($result->fetch(), $address->fetch());

        return $datas;
    }


    /**
     * Crée un account
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function create($array)
    {
        // On récupère les données pour la table address
        $zipcode = $array['zipcode'];
        $city = $array['city'];
        $country = $array['country'];
        $number = $array['number'];
        $name = $array['name'];
        $Type_id = $array['Type_id'];

        // On rempli la table Address
        $address = array('zipcode' => $zipcode, 'city' => $city, 'country' => $country, 'number' => $number, 'name' => $name, 'Type_id' => $Type_id);
        $query1 = "INSERT INTO Address(zipcode, city, country, number, name, Type_id) VALUES (:zipcode, :city, :country, :number, :name, :Type_id)";

        $requete_preparee = $this->getPdo()->prepare($query1);
        $requete_preparee->execute($address);

        //Récupération de l'ID de l'address (foreign key)
        $AddressNewid = $this->getPdo()->lastInsertId();

        // On récupère les données pour la table Account
        $LastName = $array['Name'];
        $FirstName = $array['FirstName'];
        $Birthday = $array['Birthday'];
        $Email = $array['Email'];
        $Password = $array['Password'];
        $activate = $array['activate'];
        $Role_id = $array['Role_id'];


        // On rempli la table Account
        $account = array('Name' => $LastName, 'FirstName' => $FirstName, 'Birthday' => $Birthday, 'Email' => $Email, 'Password' => $Password, 'activate' => $activate, 'Address_id' => $AddressNewid, 'Role_id' => $Role_id);

        $query2 = "INSERT INTO Account(Name, FirstName, Birthday, Email, Password, activate, Address_id, Role_id ) VALUES(:Name, :FirstName, :Birthday, :Email, :Password, :activate, :Address_id, :Role_id)";
        $result = $this->getPdo()->prepare($query2);
        return $result->execute($account);
    }



    /**
     * Crée un account pour le RH
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function createAdministrator($array)
    {
        // On récupère les données pour la table address
        $zipcode = $array['zipcode'];
        $city = $array['city'];
        $country = $array['country'];
        $number = $array['number'];
        $name = $array['name'];
        $Type_id = $array['Type_id'];


        // On rempli la table Address
        $address = array('zipcode' => $zipcode, 'city' => $city, 'country' => $country, 'number' => $number, 'name' => $name, 'Type_id' => $Type_id);
        $query1 = "INSERT INTO Address(zipcode, city, country, number, name, Type_id) VALUES (:zipcode, :city, :country, :number, :name, :Type_id)";

        $requete_preparee = $this->getPdo()->prepare($query1);
        $requete_preparee->execute($address);

        //Récupération de l'ID de l'address (foreign key)
        $AddressNewid = $this->getPdo()->lastInsertId();

        // On récupère les données pour la table Account
        $LastName = $array['Name'];
        $FirstName = $array['FirstName'];
        $Birthday = $array['Birthday'];
        $Email = $array['Email'];
        $Password = $array['Password'];
        $activate = $array['activate'];
        $Role_id = $array['Role_id'];


        // On rempli la table Account
        $account = array('Name' => $LastName, 'FirstName' => $FirstName, 'Birthday' => $Birthday, 'Email' => $Email, 'Password' => $Password, 'activate' => $activate, 'Address_id' => $AddressNewid, 'Role_id' => $Role_id);

        $query2 = "INSERT INTO Account(Name, FirstName, Birthday, Email, Password, activate, Address_id, Role_id ) VALUES(:Name, :FirstName, :Birthday, :Email, :Password, :activate, :Address_id, :Role_id)";
        $result = $this->getPdo()->prepare($query2);
        $result->execute($account);


        //Récupération de l'ID de l'address (foreign key)
        $AccountNewid = $this->getPdo()->lastInsertId();

        // On rempli la table Administrator
        $Administrator = array('Salaried_Account_id' => $AccountNewid);

        $queryAdministrator = "INSERT INTO Administrator (Salaried_Account_id) VALUES(:Salaried_Account_id)";
        $reqAdministrator = $this->getPdo()->prepare($queryAdministrator);
        $reqAdministrator->execute($Administrator);


        // On rempli la table Salaried
        $Salaried = array('Account_id' => $AccountNewid);
        $querySalaried = "INSERT INTO Salaried (Account_id) VALUES (:Account_id)";
        $reqSalaried = $this->getPdo()->prepare($querySalaried);
        return $reqSalaried->execute($Salaried);
    }



    /**
     * Crée un account pour le stagiare en formation
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function createStudent($array)
    {
        // On récupère les données pour la table address
        $zipcode = $array['zipcode'];
        $city = $array['city'];
        $country = $array['country'];
        $number = $array['number'];
        $name = $array['name'];
        $Type_id = $array['Type_id'];


        // On rempli la table Address
        $address = array('zipcode' => $zipcode, 'city' => $city, 'country' => $country, 'number' => $number, 'name' => $name, 'Type_id' => $Type_id);
        $query1 = "INSERT INTO Address(zipcode, city, country, number, name, Type_id) VALUES (:zipcode, :city, :country, :number, :name, :Type_id)";

        $requete_preparee = $this->getPdo()->prepare($query1);
        $requete_preparee->execute($address);

        //Récupération de l'ID de l'address (foreign key)
        $AddressNewid = $this->getPdo()->lastInsertId();

        // On récupère les données pour la table Account
        $LastName = $array['Name'];
        $FirstName = $array['FirstName'];
        $Birthday = $array['Birthday'];
        $Email = $array['Email'];
        $Password = $array['Password'];
        $activate = $array['activate'];
        $Role_id = $array['Role_id'];


        // On rempli la table Account
        $account = array('Name' => $LastName, 'FirstName' => $FirstName, 'Birthday' => $Birthday, 'Email' => $Email, 'Password' => $Password, 'activate' => $activate, 'Address_id' => $AddressNewid, 'Role_id' => $Role_id);

        $query2 = "INSERT INTO Account(Name, FirstName, Birthday, Email, Password, activate, Address_id, Role_id ) VALUES(:Name, :FirstName, :Birthday, :Email, :Password, :activate, :Address_id, :Role_id)";
        $result = $this->getPdo()->prepare($query2);
        $result->execute($account);


        //Récupération de l'ID de l'address (foreign key)
        $AccountNewid = $this->getPdo()->lastInsertId();


        $Training_id = $array['Training_id'];

        // On rempli la table Student
        $Student = array('Trainee_Account_id' => $AccountNewid, 'Training_id' => $Training_id);

        $queryStudent = "INSERT INTO Student (Trainee_Account_id, Training_id) VALUES(:Trainee_Account_id, :Training_id)";
        $reqStudent = $this->getPdo()->prepare($queryStudent);
        $reqStudent->execute($Student);


        // On rempli la table Trainee
        $Trainee = array('Account_id' => $AccountNewid);
        $queryTrainee = "INSERT INTO Trainee (Account_id) VALUES (:Account_id)";
        $reqTrainee = $this->getPdo()->prepare($queryTrainee);
        return $reqTrainee->execute($Trainee);
    }


    /**
     * Crée un account pour le stagiaire en accompagnement
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function createAccompagniment($array)
    {
        // On récupère les données pour la table address
        $zipcode = $array['zipcode'];
        $city = $array['city'];
        $country = $array['country'];
        $number = $array['number'];
        $name = $array['name'];
        $Type_id = $array['Type_id'];


        // On rempli la table Address
        $address = array('zipcode' => $zipcode, 'city' => $city, 'country' => $country, 'number' => $number, 'name' => $name, 'Type_id' => $Type_id);
        $query1 = "INSERT INTO Address(zipcode, city, country, number, name, Type_id) VALUES (:zipcode, :city, :country, :number, :name, :Type_id)";

        $requete_preparee = $this->getPdo()->prepare($query1);
        $requete_preparee->execute($address);

        //Récupération de l'ID de l'address (foreign key)
        $AddressNewid = $this->getPdo()->lastInsertId();

        // On récupère les données pour la table Account
        $LastName = $array['Name'];
        $FirstName = $array['FirstName'];
        $Birthday = $array['Birthday'];
        $Email = $array['Email'];
        $Password = $array['Password'];
        $activate = $array['activate'];
        $Role_id = $array['Role_id'];


        // On rempli la table Account
        $account = array('Name' => $LastName, 'FirstName' => $FirstName, 'Birthday' => $Birthday, 'Email' => $Email, 'Password' => $Password, 'activate' => $activate, 'Address_id' => $AddressNewid, 'Role_id' => $Role_id);

        $query2 = "INSERT INTO Account(Name, FirstName, Birthday, Email, Password, activate, Address_id, Role_id ) VALUES(:Name, :FirstName, :Birthday, :Email, :Password, :activate, :Address_id, :Role_id)";
        $result = $this->getPdo()->prepare($query2);
        $result->execute($account);


        //Récupération de l'ID de l'address (foreign key)
        $AccountNewid = $this->getPdo()->lastInsertId();


        $Coach_Salaried_Account_id = $array['Coach_Salaried_Account_id'];

        // On rempli la table Accompagniment
        $Accompagniment = array('Trainee_Account_id' => $AccountNewid, 'Coach_Salaried_Account_id' => $Coach_Salaried_Account_id);

        $queryAccompagniment = "INSERT INTO Accompagniment (Trainee_Account_id, Coach_Salaried_Account_id) VALUES(:Trainee_Account_id, :Coach_Salaried_Account_id)";
        $reqAccompagniment = $this->getPdo()->prepare($queryAccompagniment);
        $reqAccompagniment->execute($Accompagniment);


        // On rempli la table Trainee
        $Trainee = array('Account_id' => $AccountNewid);
        $queryTrainee = "INSERT INTO Trainee (Account_id) VALUES (:Account_id)";
        $reqTrainee = $this->getPdo()->prepare($queryTrainee);
        return $reqTrainee->execute($Trainee);
    }

    /**
     * Crée un account pour le salarié accompagnateur
     *
     * @param  mixed $array
     *
     * @return boolean
     */    public function createCoach($array)
    {
        // On récupère les données pour la table address
        $zipcode = $array['zipcode'];
        $city = $array['city'];
        $country = $array['country'];
        $number = $array['number'];
        $name = $array['name'];
        $Type_id = $array['Type_id'];


        // On rempli la table Address
        $address = array('zipcode' => $zipcode, 'city' => $city, 'country' => $country, 'number' => $number, 'name' => $name, 'Type_id' => $Type_id);
        $query1 = "INSERT INTO Address(zipcode, city, country, number, name, Type_id) VALUES (:zipcode, :city, :country, :number, :name, :Type_id)";

        $requete_preparee = $this->getPdo()->prepare($query1);
        $requete_preparee->execute($address);

        //Récupération de l'ID de l'address (foreign key)
        $AddressNewid = $this->getPdo()->lastInsertId();

        // On récupère les données pour la table Account
        $LastName = $array['Name'];
        $FirstName = $array['FirstName'];
        $Birthday = $array['Birthday'];
        $Email = $array['Email'];
        $Password = $array['Password'];
        $activate = $array['activate'];
        $Role_id = $array['Role_id'];


        // On rempli la table Account
        $account = array('Name' => $LastName, 'FirstName' => $FirstName, 'Birthday' => $Birthday, 'Email' => $Email, 'Password' => $Password, 'activate' => $activate, 'Address_id' => $AddressNewid, 'Role_id' => $Role_id);

        $query2 = "INSERT INTO Account(Name, FirstName, Birthday, Email, Password, activate, Address_id, Role_id ) VALUES(:Name, :FirstName, :Birthday, :Email, :Password, :activate, :Address_id, :Role_id)";
        $result = $this->getPdo()->prepare($query2);
        $result->execute($account);


        //Récupération de l'ID de l'address (foreign key)
        $AccountNewid = $this->getPdo()->lastInsertId();


        // On rempli la table Coach
        $Coach = array('Salaried_Account_id' => $AccountNewid);

        $queryCoach = "INSERT INTO Coach (Salaried_Account_id) VALUES(:Salaried_Account_id)";
        $reqCoach = $this->getPdo()->prepare($queryCoach);
        $reqCoach->execute($Coach);


        // On rempli la table Salaried
        $Salaried = array('Account_id' => $AccountNewid);
        $querySalaried = "INSERT INTO Salaried (Account_id) VALUES (:Account_id)";
        $reqSalaried = $this->getPdo()->prepare($querySalaried);
        return $reqSalaried->execute($Salaried);
    }

    /**
     * Crée un account pour le salarié RH
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function createCoordinator($array)
    {
        // On récupère les données pour la table address
        $zipcode = $array['zipcode'];
        $city = $array['city'];
        $country = $array['country'];
        $number = $array['number'];
        $name = $array['name'];
        $Type_id = $array['Type_id'];


        // On rempli la table Address
        $address = array('zipcode' => $zipcode, 'city' => $city, 'country' => $country, 'number' => $number, 'name' => $name, 'Type_id' => $Type_id);
        $query1 = "INSERT INTO Address(zipcode, city, country, number, name, Type_id) VALUES (:zipcode, :city, :country, :number, :name, :Type_id)";

        $requete_preparee = $this->getPdo()->prepare($query1);
        $requete_preparee->execute($address);

        //Récupération de l'ID de l'address (foreign key)
        $AddressNewid = $this->getPdo()->lastInsertId();

        // On récupère les données pour la table Account
        $LastName = $array['Name'];
        $FirstName = $array['FirstName'];
        $Birthday = $array['Birthday'];
        $Email = $array['Email'];
        $Password = $array['Password'];
        $activate = $array['activate'];
        $Role_id = $array['Role_id'];


        // On rempli la table Account
        $account = array('Name' => $LastName, 'FirstName' => $FirstName, 'Birthday' => $Birthday, 'Email' => $Email, 'Password' => $Password, 'activate' => $activate, 'Address_id' => $AddressNewid, 'Role_id' => $Role_id);

        $query2 = "INSERT INTO Account(Name, FirstName, Birthday, Email, Password, activate, Address_id, Role_id ) VALUES(:Name, :FirstName, :Birthday, :Email, :Password, :activate, :Address_id, :Role_id)";
        $result = $this->getPdo()->prepare($query2);
        $result->execute($account);


        //Récupération de l'ID de l'address (foreign key)
        $AccountNewid = $this->getPdo()->lastInsertId();

        // On rempli la table Coordinator
        $Coordinator = array('Salaried_Account_id' => $AccountNewid);

        $queryCoordinator = "INSERT INTO Coordinator (Salaried_Account_id) VALUES(:Salaried_Account_id)";
        $reqCoordinator = $this->getPdo()->prepare($queryCoordinator);
        $reqCoordinator->execute($Coordinator);


        // On rempli la table Salaried
        $Salaried = array('Account_id' => $AccountNewid);
        $querySalaried = "INSERT INTO Salaried (Account_id) VALUES (:Account_id)";
        $reqSalaried = $this->getPdo()->prepare($querySalaried);
        return $reqSalaried->execute($Salaried);
    }

    /**
     * Crée un account pour l'entreprise partenaire
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function createCompany($array)
    {
        // On récupère les données pour la table address
        $zipcode = $array['zipcode'];
        $city = $array['city'];
        $country = $array['country'];
        $number = $array['number'];
        $name = $array['name'];
        $Type_id = $array['Type_id'];


        // On rempli la table Address
        $address = array('zipcode' => $zipcode, 'city' => $city, 'country' => $country, 'number' => $number, 'name' => $name, 'Type_id' => $Type_id);
        $query1 = "INSERT INTO Address(zipcode, city, country, number, name, Type_id) VALUES (:zipcode, :city, :country, :number, :name, :Type_id)";

        $requete_preparee = $this->getPdo()->prepare($query1);
        $requete_preparee->execute($address);

        //Récupération de l'ID de l'address (foreign key)
        $AddressNewid = $this->getPdo()->lastInsertId();

        // On récupère les données pour la table Account
        $LastName = $array['Name'];
        $FirstName = $array['FirstName'];
        $Birthday = $array['Birthday'];
        $Email = $array['Email'];
        $Password = $array['Password'];
        $activate = $array['activate'];
        $Role_id = $array['Role_id'];


        // On rempli la table Account
        $account = array('Name' => $LastName, 'FirstName' => $FirstName, 'Birthday' => $Birthday, 'Email' => $Email, 'Password' => $Password, 'activate' => $activate, 'Address_id' => $AddressNewid, 'Role_id' => $Role_id);

        $query2 = "INSERT INTO Account(Name, FirstName, Birthday, Email, Password, activate, Address_id, Role_id ) VALUES(:Name, :FirstName, :Birthday, :Email, :Password, :activate, :Address_id, :Role_id)";
        $result = $this->getPdo()->prepare($query2);
        $result->execute($account);


        //Récupération de l'ID de l'address (foreign key)
        $AccountNewid = $this->getPdo()->lastInsertId();

        $Coach_Salaried_Account_id = $array['Coach_Salaried_Account_id'];

        // On rempli la table Company
        $Company = array('Account_id' => $AccountNewid, 'Coach_Salaried_Account_id' => $Coach_Salaried_Account_id);
        $queryCompany = "INSERT INTO Company (Account_id, Coach_Salaried_Account_id) VALUES (:Account_id, :Coach_Salaried_Account_id)";
        $reqCompany = $this->getPdo()->prepare($queryCompany);
        return $reqCompany->execute($Company);
    }



    /**
     * mise a jour d'un account avec son id
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function update($array)
    {

        $id = $array['id'];
        $zipcode = $array['zipcode'];
        $city = $array['city'];
        $country = $array['country'];
        $number = $array['number'];
        $nameAddress = $array['name'];
        $Type_id = $array['Type_id'];
        $LastName = $array['Name'];
        $FirstName = $array['FirstName'];
        $Birthday = $array['Birthday'];
        $Email = $array['Email'];
        $Password = $array['Password'];
        $activate = $array['activate'];
        $Role_id = $array['Role_id'];


        // On rempli la table Account
        $accountArray = array('id' => $id, 'Name' => $LastName, 'FirstName' => $FirstName, 'Birthday' => $Birthday, 'Email' => $Email, 'Password' => $Password,   'Role_id' => $Role_id, 'activate' => $activate, 'zipcode' => $zipcode, 'city' => $city, 'country' => $country, 'number' => $number, 'name' => $nameAddress, 'Type_id' => $Type_id);


        $query = "UPDATE Account as ac JOIN Address as ad ON ac.Address_id = ad.id JOIN Type as ty ON ad.Type_id = ty.id SET ac.id ='" . $id . "',ac.Name ='" . $LastName . "', ac.FirstName ='" . $FirstName . "', ac.Birthday ='" . $Birthday . "', ac.Email ='" . $Email . "', ac.Password ='" . $Password . "',ac.Role_id ='" . $Role_id . "', ac.activate ='" . $activate . "',  ad.zipcode='" . $zipcode . "',ad.city='" . $city . "', ad.country='" . $country . "', ad.number='" . $number . "', ad.name='" . $nameAddress . "', ad.Type_id='" . $Type_id . "' WHERE ac.id ='" . $id . "'";


        $result = $this->getPdo()->prepare($query);
        echo ($result->execute($accountArray)) ? "true" : "false";
    }


    /**
     * rechercher account par email et password.
     *
     * @param  mixed $email
     * @param  mixed $password
     *
     * @return array
     */
    public function getLogin($email, $password)
    {
        $query = "SELECT * FROM Account WHERE Email='" . $email . "' AND Password='" . $password . "'";
        $result = $this->getPdo()->query($query);
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Account');
        return $result->fetch();
    }

    /**
     * retrouve l'account par l'email
     *
     * @param  mixed $email
     *
     * @return array
     */
    public function getFullUser($email)
    {
        $result = $this->getPdo()->query("SELECT * FROM Account WHERE Email='" . $email . "'");
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Account');
        return $result->fetch();
    }


    public function delete($id)
    { }
    public function getAllBy($filter)
    { }
}
