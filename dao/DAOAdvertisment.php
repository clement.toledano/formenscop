<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;

class DAOAdvertisment extends DAO
{


    /**
     * Récupère les annonces des entreprises partenaires
     *
     * @return array
     */
    public function getAll()
    {
        $query = "SELECT ad.id, ad.date, ad.title, ad.description, ad.Company_id, ad.Contract_id FROM Advertisment as ad JOIN Contract as co ON ad.Contract_id = co.id JOIN Company as comp ON comp.id = ad.Company_id JOIN Account as ac ON comp.Account_id = ac.id";
        $result = $this->getPdo()->query($query);
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Advertisment');
        return $result->fetchAll();

    }


    public function getAllBy($filter)
    { }
    public function create($array)
    { }
    public function delete($id)
    { }
    public function retrieve($id)
    { }
    public function update($array)
    { }
}
