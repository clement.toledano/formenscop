<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;

class DAOCompany extends DAO
{
    public function getAll()
    {
        $result = $this->getPdo()->query('SELECT * FROM Company');
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Company');
        return $result->fetchAll();
    }

    public function retrieve($id)
    {
        $result = $this->getPdo()->query("SELECT * FROM Company WHERE id='" . $id . "'");
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Company');
        return $result->fetch();
    }

    public function create($array)
    { }
    public function getAllBy($filter)
    { }
    public function delete($id)
    { }
    public function update($array)
    { }
}
