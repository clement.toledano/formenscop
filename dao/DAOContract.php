<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;

class DAOContract extends DAO
{
    public function getAll()
    {
        $query = "SELECT * FROM Contract";
        $result = $this->getPdo()->query($query);
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Contract');
        return $result->fetchAll();
    }

    public function retrieve($id)
    {
        $result = $this->getPdo()->query("SELECT * FROM Contract WHERE id='" . $id . "'");
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Contract');
        return $result->fetch();
    }

    public function getAllBy($filter)
    { }
    public function create($array)
    { }

    public function delete($id)
    { }
    public function update($array)
    { }
}
