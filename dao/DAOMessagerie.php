<?php

namespace BWB\Framework\mvc\dao;

use PDO;
use  BWB\Framework\mvc\DAO;

class DAOMessagerie extends DAO
{

    public function getAll()
    { }

    /**
     * récupère les 10 derniers messages non effacés
     *
     * @param  mixed $data
     *
     * @return array
     */
    public function getAllBy($data)
    {
        // On récupère les données envoyées via la messagerie
        $id_sender = $data["id_sender"];
        $id_receivers = $data["id_receivers"];
        $archives = [];
        $receiver_Color = 0;

        foreach ($id_receivers as $cle => $val) {
            $receiver_Color++;
            $id_receiver = $val;
            $result = $this->getPdo()->query("SELECT Message.id AS msgId, Account_id_sender,Account_id_receiver, subject, texte, date, Account.FirstName AS firstName, Account.Name AS name 
            FROM Message, Communication, Account 
            WHERE (Message_id = Message.id AND Account.id = Account_id_receiver AND Account_id_sender = $id_sender AND Account_id_receiver = $id_receiver AND Message.removed = '' )
            OR (Message_id = Message.id AND Account.id = Account_id_sender AND Account_id_sender = $id_receiver AND Account_id_receiver = $id_sender AND Message.removed = '') ORDER BY Message.id DESC LIMIT 10");
            // PDO::FETCH_ASSOC signifie qu'on veut que $row soit un tableau associatif
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                $row['receiver_Color'] = $receiver_Color;
                array_push($archives, $row);
            }
        }
        return $archives;
    }

    /**
     * Requête qui récupère tous les utilisateurs sauf le RH
     *
     * @return array
     */
    public function listeUsers()
    {
        $result = $this->getPdo()->query('SELECT * FROM Account WHERE Role_id!="1"');
        $listUsers = [];
        while ($row = $result->fetch()) {
            array_push($listUsers, $row);
        }
        return $listUsers;
    }

    /**
     * Envoi d'un message 
     *
     * @param  mixed $data
     *
     * @return boolean
     */
    public function create($data)
    {
        // On récupère les données envoyées via la messagerie
        $texte = $data["texte"];
        $subject = $data["subject"];
        $id_sender = $data["id_sender"];

        $id_receivers = $data["id_receivers"];

        foreach ($id_receivers as $cle => $val) {

            $id_receiver = $val;

            // On rempli la table Message
            $valMessage = ['subject' => $subject, 'texte' => $texte];
            $requete = "INSERT INTO Message (subject, texte, removed) VALUES (:subject, :texte, '')";
            $requete_preparee = $this->getPdo()->prepare($requete);
            $requete_preparee->execute($valMessage);

            //Récupération de l'ID du dernier message envoyé (foreign key)
            $Message_id = $this->getPdo()->lastInsertId();

            // On rempli la table Communication
            $valCommunication = ['id_receiver' => $id_receiver, 'id_sender' => $id_sender, 'Message_id' => $Message_id];
            $requete = "INSERT INTO Communication (Account_id_sender, Account_id_receiver, Message_id) VALUES (:id_sender, :id_receiver, :Message_id)";
            $requete_preparee = $this->getPdo()->prepare($requete);
            $requete_preparee->execute($valCommunication);

            //Récupération de l'ID de la communication
            $Communication_id = $this->getPdo()->lastInsertId();

            // On rempli la table messageState
            $valState = ['Communication_id' => $Communication_id];
            $requete = "INSERT INTO messageState (Communication_id) VALUES (:Communication_id)";

            $requete_preparee = $this->getPdo()->prepare($requete);
            $requete_preparee->execute($valState);
        }
    }


    /**
     * Efface un message
     *
     * @param  mixed $data
     *
     * @return boolean
     */
    public function eraseMsg($data)
    {
        // On récupère les données envoyées via la messagerie
        $msgId = $data["msgId"];

        // On rempli la table Message
        $valRequest = ['id' => $msgId];
        $requete = "UPDATE Message SET removed='true' WHERE id=:id";
        $requete_preparee = $this->getPdo()->prepare($requete);
        $requete_preparee->execute($valRequest);
    }

    public function retrieve($id)
    { }

    public function update($array)
    { }

    public function delete($id)
    { }
}
