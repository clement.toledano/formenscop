<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;

class DAOModule extends DAO
{

    /**
     * crée un nouveau module
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function create($array)
    {
        $name = $array['name'];
        $desciption = $array['desciption'];
        $volume = $array['volume'];
        $Training_id = $array['Training_id'];

        $module = array('name' => $name, 'desciption' => $desciption, 'volume' => $volume, 'Training_id' => $Training_id);
        $query = "INSERT INTO Module(id, name, desciption,  volume, Training_id) 
                VALUES (NULL, :name, :desciption, :volume, :Training_id)";

        $requete_preparee = $this->getPdo()->prepare($query);
        return $requete_preparee->execute($module);
    }


    /**
     * affiche la liste des modules
     *
     * @return array
     */
    public function getAll()
    {
        $modul = $this->getPdo()->query('SELECT Module.id, Module.name, Module.desciption, Module.start, Module.volume, Training.name AS Tname FROM Module, Training WHERE Module.Training_id = Training.id AND Module.name !="delete"');
        $modul->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Module');

        $training = $this->getPdo()->query('SELECT * FROM Training');
        $training->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Training');

        $result = array($modul->fetchAll(), $training->fetchAll());
        return $result;
    }

    /**
     * affiche le détails des modules par l'id
     *
     * @param  mixed $id
     *
     * @return array
     */
    public function getAllbyId($id)
    {
        $id = $id['edit'];
        $response = $this->getPdo()->query("SELECT Module.id AS id, Module.name AS Mname, Module.desciption AS Mdesciption,
         Module.start AS Mstart, Module.volume AS Mvolume, Training.name AS Tname, Training.start AS Tstart, Training.end AS Tend, 
         Training.volume AS Tvolume FROM Module, Training WHERE Module.Training_id = Training.id AND Module.id=" . $id);
        $res = $response->fetchAll();
        return $res;
    }


    /**
     * edite un module
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function update($array)
    {
        $id = $array["id"];
        $name = $array['name'];
        $desciption = $array['desciption'];
        $volume = $array['volume'];
        $training_id = $array['Training_id'];

        $query = "UPDATE Module SET name='" . $name . "', desciption='" . $desciption . "', volume='" . $volume . "', Training_id='" . $training_id . "' WHERE id=" . $id;
        $req = $this->getPdo()->prepare($query);
        return $req->execute();
    }

    /**
     * methode UPDATE pour cacher les modules "delete" 
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function deleteModule($array)
    {
        $id = $array["id"];
        $query = "UPDATE Module SET name='delete' WHERE id=" . $id;
        $req = $this->getPdo()->prepare($query);
        return $req->execute();
    }



    /**
     * affiche le module par son id
     *
     * @param  mixed $id
     *
     * @return array
     */
    public function retrieve($id)
    {
        $id = $id['edit'];
        $query = "SELECT Module.id AS id, Module.name, Module.desciption, Module.start, Module.volume, Module.Training_id, Training.name AS Tname FROM Module, Training WHERE Module.Training_id = Training.id AND Module.name !='delete' AND Module.id=" . $id;
        $modul = $this->getPdo()->query($query);
        $modul->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Module');
        return $modul->fetch();
    }

    public function delete($id)
    { }

    public function getAllBy($filter)
    { }
}
