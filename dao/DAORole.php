<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;


class DAORole extends DAO
{
    /**
     * Retourne l'ensemble des roles
     *
     * @return array
     */
    public function getAll()
    {
        $result = $this->getPdo()->query('SELECT * FROM Role');
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Role');
        $res = $result->fetchAll();
        return $res;
    }


    /**
     * Retourne les roles qui sont lié à des comptes clients
     *
     * @return array
     */
    public function getAllRoleUsed()
    {
        $result = $this->getPdo()->query('SELECT * FROM Role WHERE id IN (SELECT Role_id FROM Account)');
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Role');
        $res = $result->fetchAll();
        return $res;
    }


    /**
     * retrouve un role par son id
     *
     * @param  mixed $id
     *
     * @return array
     */
    public function retrieve($id)
    {
        $query = "SELECT * FROM Role WHERE id=" . $id;
        $result = $this->getPdo()->query($query);
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Role');
        return $result->fetch();
    }


   
    /**
     * récupérer le rôle en fonction de role_id  de l'account ($row)
     *
     * @param  mixed $roleId
     *
     * @return string
     */
    public function getRole($roleId)
    {
        $result = $this->getPdo()->query("SELECT designiation FROM Role WHERE id='" . $roleId . "'");
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Role');
        $role = $result->fetch();
        return $role;
    }

    /**
     * crée un role
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function create($array)
    {
        // On récupère les données pour la table address
        $designiation = $array['designiation'];
        $description = $array['description'];

        // On rempli la table Address
        $role = array('designiation' => $designiation, 'description' => $description);
        $query = "INSERT INTO Role (designiation, description) VALUES (:designiation, :description)";

        $requete_preparee = $this->getPdo()->prepare($query);
        return $requete_preparee->execute($role);
    }


    /**
     * met a jour un role
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function update($array)
    {
        // On récupère les données pour la table address
        $id = $array['id'];
        $designiation = $array['designiation'];
        $description = $array['description'];

        // On rempli la table Address
        $role = array('designiation' => $designiation, 'description' => $description);

        $query = "UPDATE Role SET designiation='" . $designiation . "',description='" . $description . "' WHERE id=" . $id;
        $requete_preparee = $this->getPdo()->prepare($query);
        return $requete_preparee->execute($role);
    }

    /**
     * Supprime un role
     *
     * @param  mixed $id
     *
     * @return boolean
     */
    public function delete($id)
    {
        $query = "DELETE FROM Role WHERE id=" . $id;
        $requete_preparee = $this->getPdo()->prepare($query);
        return $requete_preparee->execute();
    }
    public function getAllBy($filter)
    { }
}
