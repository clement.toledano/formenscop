<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\dao\DAOAccompagniment;
use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\models\Coach;
use BWB\Framework\mvc\models\Trainee;

class Accompagniment extends DefaultModel
{
    protected $Trainee_Account_id;
    protected $Coach_Salaried_Account_id;

    public function __construct($Trainee_Account_id = null)
    {
        if (!is_null($Trainee_Account_id)) {
            $this->parse((new DAOAccompagniment())->retrieve($Trainee_Account_id));
        }
    }


    /** GETTERS */

    function getTrainee_Account_id()
    {
        return $this->Trainee_Account_id;
    }

    function getCoach_Salaried_Account_id()
    {
        return $this->Coach_Salaried_Account_id;
    }

  


    /** SETTERS */


    public function setTrainee_Account_id($Trainee_Account_id)
    {
        $this->Trainee_Account_id = new Trainee($Trainee_Account_id);
    }

    public function setCoach_Salaried_Account_id($Coach_Salaried_Account_id)
    {
       $this->Coach_Salaried_Account_id = new Coach($Coach_Salaried_Account_id);
    }
}
