<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\models\Account;
use BWB\Framework\mvc\models\Message;
use BWB\Framework\mvc\dao\DAOCommunication;

class Communication extends DefaultModel
{
    protected $id;
    protected $Account_id_sender;
    protected $Account_id_receiver;
    protected $Message_id;

    public function __construct($id)
    {
        if(!is_null($id)){

            $this->parse((new DAOCommunication())->retrieve($id));
        }
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setAccount_id_sender($id_sender)
    {
        $this->Account_id_sender = new Account($id_sender);
    }

    public function getAccount_id_sender()
    {
        return $this->Account_id_sender;
    }

    public function setAccount_id_receiver($id_receiver)
    {
        $this->Account_id_receiver = new Account($id_receiver);
    }

    public function getAccount_id_receiver()
    {
        return $this->Account_id_receiver;
    }

    public function setMessage_id($Message_id)
    {
        $this->Message_id = new Message($Message_id);
    }

    public function getMessage_id()
    {
        return $this->Message_id;
    }
}