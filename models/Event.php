<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOEvent;

class Event extends DefaultModel
{
    protected $id;
    protected $date_init;
    protected $date_end;
    protected $name;
    protected $description;

    public function __construct($id = null)
    {
        if(!is_null($id)){
            
            $this->parse((new DAOEvent())->retrieve($id));
        }
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDate_init($date_init)
    {
        $this->date_init = $date_init;
    }

    public function getDate_init()
    {
        return $this->date_init;
    }

    public function setDate_end($date_end)
    {
        $this->date_end = $date_end;
    }

    public function getDate_end()
    {
        return $this->date_end;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }
}