<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\models\Training;
use BWB\Framework\mvc\dao\DAOModule;

class Module extends DefaultModel
{
    protected $id;
    protected $name;
    protected $desciption;
    protected $start;
    protected $end;
    protected $volume;
    protected $Training_id;

    public function __construct($id = null)
    {
        if(!is_null($id)){
        $this->parse((new DAOModule())->retrieve($id));}
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDesciption($desciption)
    {
        $this->desciption = $desciption;
    }

    public function getDesciption()
    {
        return $this->desciption;
    }

    public function setStart($start)
    {
        $this->start = $start;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function setVolume($volume)
    {
        $this->volume = $volume;
    }

    public function getVolume()
    {
        return $this->volume;
    }

    public function setTraining_id($Training_id)
    {
        $this->Training_id = new Training($Training_id);
    }

    public function getTraining_id()
    {
        return $this->Training_id;
    }
    
    

    

}