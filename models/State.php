<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOState;
use BWB\Framework\mvc\models\Communication;

class State extends DefaultModel
{
    protected $Communication_id;
    protected $state;

    public function __construct($id = null)
    {
        if(!is_null($id)){
        $this->parse((new DAOState())->retrieve($id));
        }
    }

    public function setCommunication_id($Communication_id)
    {
        $this->Communication_id = new Communication($Communication_id);
    }

    public function getCommunication_id()
    {
        return $this->Communication_id;
    }

    public function setState($state)
    {
        $this->state = $state;
    }

    public function getStated()
    {
        return $this->state;
    }
}