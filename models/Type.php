<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOType;

class Type extends DefaultModel
{
    protected $id;
    protected $name;

    public function __construct($id = null)
    {
        if(!is_null($id)){
        $this->parse((new DAOType())->retrieve($id));
        }
    }




    /** GETTERS */
    function getId()
    {
        return $this->id;
    }
    function getName()
    {
        return $this->name;
    }
    

    /** SETTERS */

    function setId($id)
    {
        $this->id = $id;
    }
    function setName($name)
    {
        $this->name = $name;
    }
  
}
