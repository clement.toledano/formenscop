<!DOCTYPE html>
<html>

<head>
	<?php include('include/head.php'); ?>
</head>

<body>
	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">

		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Create a account</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/home">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Create a account</li>
								</ol>
							</nav>
						</div>
						<div class="col-md-6 col-sm-12 text-right">
							<div class="dropdown">
								<a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
									Juillet 2019
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="#">Export List</a>
									<a class="dropdown-item" href="#">Policies</a>
									<a class="dropdown-item" href="#">View Assets</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Default Basic Forms Start -->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<!-- FORMULAIRE -->
					<!-- FORMULAIRE -->
					<!-- FORMULAIRE -->
					<form id="create_account_form" action="/account/" method="post">

						<div class="form-group col-md-4 alert alert-danger">
							<label for="inputActive">Compte actif ?</label>
							<select id="inputActive" class="form-control" name="activate">
								<option value="true" selected>Oui</option>
								<option value="false">Non</option>

							</select>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Nom</label>
							<div class="col-sm-12 col-md-10">
								<input class="form-control" type="text" placeholder="Johnny" name="Name" value="MAXXX">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Prenom</label>
							<div class="col-sm-12 col-md-10">
								<input class="form-control" type="text" placeholder="Brown" name="FirstName" value="WELLL">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Date de Naissance</label>
							<div class="form-group col-md-10">
								<input class="form-control date-picker" placeholder="Select Date" type="text" name="Birthday" value="1221-12-21">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Email</label>
							<div class="col-sm-12 col-md-10">
								<input class="form-control" placeholder="bootstrap@example.com" type="email" name="Email" value="bootstrap@example.com">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Password</label>
							<div class="col-sm-12 col-md-10">
								<input class="form-control" placeholder="password" type="password" name="Password" value="1234">
							</div>
						</div> <br>

						<div class="form-row">
							<div class="form-group col-md-2">
								<label for="inputNumber">Numero</label>
								<input type="text" class="form-control" id="inputNumber" name="number" value="99">
							</div>
							<div class="form-group col-md-2">
								<label for="inputState">Type</label>
								<select id="inputState" class="form-control" name="Type_id">

									<?php foreach ($datas[1] as $data) : ?>
										<option value="<?= $data->getId(); ?>"><?= $data->getName(); ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="form-group col-md-8">
								<label for="inputCity">Adresse</label>
								<input type="text" class="form-control" id="nomRue" placeholder="adresse" name="name" value="UUUUUUUU">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<label for="inputZip">Zipcode</label>
								<input type="text" class="form-control" id="inputZip" pattern="[0-9]{5}" name="zipcode" value="99999">
							</div>
							<div class="form-group col-md-4">
								<label for="inputCity">City</label>
								<input type="text" class="form-control" id="inputCity" name="city" value="MMMMMM">
							</div>
							<div class="form-group col-md-4">
								<label for="inputCity">Pays</label>
								<input type="text" class="form-control" id="country" placeholder="Pays" name="country" value="France">
							</div>
						</div>
						
						<div class="form-group row">
								<label class="col-sm-12 col-md-2 col-form-label">Role</label>
								<div class="col-sm-12 col-md-10">
									<select class="custom-select col-12" name="Role_id" id="Role_id" onchange="test()">
											
								<?php foreach ($datas[0] as $data) : ?>
								<option value="<?= $data->getId(); ?>"><?= $data->getDesigniation(); ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				
				<div class="form-group row" id="Training_id">
					<label class="col-sm-12 col-md-2 col-form-label">Formation</label>
					<div class="col-sm-12 col-md-10">
						<select class="custom-select col-12" name="Training_id" onchange="choixDuRole()">
								
							<?php foreach ($datas[2] as $data) : ?>
							<option value="<?= $data->getId(); ?>"><?= $data->getName(); ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group row" id="Coach_Salaried_Account_id">
					<label class="col-sm-12 col-md-2 col-form-label">Accompagnateur</label>
					<div class="col-sm-12 col-md-10">
						<select class="custom-select col-12" name="Coach_Salaried_Account_id">
							<?php foreach ($datas[3] as $data) : ?>
							<option value="<?= $data->getid(); ?>"><?= $data->getName(); ?></option>

							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group row" id="Salaried_Account_id">
					<label class="col-sm-12 col-md-2 col-form-label">Accompagnateur</label>
					<div class="col-sm-12 col-md-10">
						<select class="custom-select col-12" name="Salaried_Account_id">
							<?php foreach ($datas[3] as $data) : ?>
							<option value="<?= $data->getid(); ?>"><?= $data->getName(); ?></option>
							
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				
			</div><br>
				
				<div id="response"></div>
				<br>
				<!-- BUTTON SUBMIT -->
				<button id="button_create_account" type="button" class="btn btn-primary">Enregistrer</button>
				<!-- END BUTTON SUBMIT -->
				</form>


			</div>
			<!-- Default Basic Forms End -->

		</div>
		<?php include('include/footer.php'); ?>
	</div>
	</div>
	<?php include('include/script.php'); ?>
</body>

</html>