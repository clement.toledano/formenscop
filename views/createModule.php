<!DOCTYPE html>
<html>

<head>
	<?php include('include/head.php'); ?>
</head>

<body>
	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Create a module</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/home">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Create a module</li>
								</ol>
							</nav>
						</div>
						<div class="col-md-6 col-sm-12 text-right">
							<div class="dropdown">
								<a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
									Juillet 2019
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="#">Export List</a>
									<a class="dropdown-item" href="#">Policies</a>
									<a class="dropdown-item" href="#">View Assets</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Default Basic Forms Start -->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<!-- FORMULAIRE -->
					<!-- FORMULAIRE -->
					<!-- FORMULAIRE -->
					<form id="create_module_form" action="/module/" method="post">
						<h3>Module</h3><br><br>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Name</label>
							<div class="col-sm-12 col-md-10">
								<input value="cuisine" class="form-control" type="text" placeholder="name module" name="name">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Description</label>
							<div class="form-group col-md-10">
								<input value="le plaisir de cuisiner" class="form-control" placeholder="description module" type="text" name="desciption">
							</div>
						</div>
	

						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Volume</label>
							<div class="col-sm-12 col-md-10">
								<input value="60" class="form-control" placeholder="volume module" type="days" name="volume">
							</div>
						</div> 

						<!-- modifications formation-->
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Formation</label>
							<div class="col-sm-12 col-md-10">
								<select class="custom-select col-12" name="Training_id">

									<?php foreach ($datas as $data) : ?>
										<option value="<?= $data->getId(); ?>"><?= $data->getName(); ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>

						<div id="response"></div>
						<br>
						<!-- BUTTON SUBMIT -->
						<button id="button_create_module" type="button" class="btn btn-primary">Sauvegarder</button>

					</form>
				</div>
				<!-- Default Basic Forms End -->
			</div>

			<!-- Requete insertion dans la base de donnée -->
			<?php

			// $bdd = mysqli_connect("localhost", "phpmyadmin", "root", "formenscop");

			// if (isset($_POST['register'])) {

			// 	$name = $_POST['name'];
			// 	$desciption = $_POST['desciption'];   
			// 	$start = $_POST['start'];   
			// 	$volume = $_POST['volume'];   
			// 	$nameFormation = $_POST['nameFormation'];   
			// 	$startFormation = $_POST['startFormation'];   
			// 	$end = $_POST['end'];   
			//     $volumeFormation = $_POST['volumeFormation'];   
			//     $id= "1";

			// 	$req = "INSERT INTO `Training` (`id`, `name`, `start`, `end`, `volume`)
			//             VALUES (NULL, '".$nameFormation."', '".$startFormation."', '".$end."', '".$volumeFormation."')";

			//     $req2 = "INSERT INTO `Module`(`id`, `name`, `desciption`, `start`, `volume`, `Training_id`) 
			//     VALUES (NULL, '".$name."', '".$desciption."', '".$start."', '".$volume."', LAST_INSERT_ID())";

			// 	mysqli_query($bdd, $req);	
			// 	mysqli_query($bdd, $req2);	

			// }

			?>
			<!-- Fin requete insertion dans la base de donnée -->

			<?php include('include/footer.php'); ?>
		</div>
	</div>
	<?php include('include/script.php'); ?>
</body>

</html>