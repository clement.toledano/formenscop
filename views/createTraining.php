<!DOCTYPE html>
<html>

<head>
	<?php include('include/head.php'); ?>
</head>

<body>
	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Create a training</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/home">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Create a training</li>
								</ol>
							</nav>
						</div>
						<div class="col-md-6 col-sm-12 text-right">
							<div class="dropdown">
								<a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
									Juillet 2019
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="#">Export List</a>
									<a class="dropdown-item" href="#">Policies</a>
									<a class="dropdown-item" href="#">View Assets</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Default Basic Forms Start -->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<!-- FORMULAIRE -->
					<!-- FORMULAIRE -->
					<!-- FORMULAIRE -->
					<form id="create_training_form">
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Name</label>
							<div class="col-sm-12 col-md-10">
								<input value="aze" class="form-control" type="text" placeholder="Johnny" name="name">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Start</label>
							<div class="form-group col-md-10">
								<input value="2019-08-01" class="form-control" placeholder="2019-08-01" type="text" name="start">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">End</label>
							<div class="form-group col-md-10">
								<input value="2020-08-01" class="form-control" placeholder="2019-09-01" type="text" name="end">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Volume</label>
							<div class="col-sm-12 col-md-10">
								<input value="21" class="form-control" placeholder="days of training" type="days" name="volume">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Formateur</label>
							<div class="col-sm-12 col-md-4">
								<select class="form-control" name="Salaried_Account_id">
									<?php foreach ($datas[0] as $data) : ?>
										<option value="<?= $data->getId(); ?>"><?= $data->getName() . " " . $data->getFirstName() ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Coordinateur</label>
							<div class="col-sm-12 col-md-4">
								<select class="form-control" name="Coordinator_Salaried_Account_id">
									<?php foreach ($datas[1] as $data) : ?>
										<option value="<?= $data->getId(); ?>"><?= $data->getName() . " " . $data->getFirstName() ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<br>

						<div id="response"></div>
						<br>
						<!-- BUTTON SUBMIT -->
						<button id="button_create_training" type="button" class="btn btn-primary">Enregistrer</button>

					</form>
				</div>
				<!-- Default Basic Forms End -->
			</div>

			<!-- Requete insertion dans la base de donnée -->
			<?php

			$bdd = mysqli_connect("localhost", "phpmyadmin", "root", "formenscop");

			if (isset($_POST['register'])) {

				$name = $_POST['name'];
				$start = $_POST['start'];
				$end = $_POST['end'];
				$volume = $_POST['volume'];

				$req = "INSERT INTO `Training` (`id`, `name`, `start`, `end`, `volume`)
						VALUES (NULL, '" . $name . "', '" . $start . "', '" . $end . "', '" . $volume . "')";
				mysqli_query($bdd, $req);
			}

			?>
			<!-- Fin requete insertion dans la base de donnée -->

			<?php include('include/footer.php'); ?>
		</div>
	</div>
	<?php include('include/script.php'); ?>
</body>

</html>