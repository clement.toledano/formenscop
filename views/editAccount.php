<?php include('include/head.php'); ?>

<?php include('include/header.php'); ?>
<?php include('include/sidebar.php'); ?>
<div class="main-container">
	<div class="pd-ltr-20 xs-pd-20-10">
		<div class="min-height-200px">
			<div class="page-header">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="title">
							<h4>Edit account</h4>
						</div>
						<nav aria-label="breadcrumb" role="navigation">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="/home">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">edit account</li>
							</ol>
						</nav>
					</div>
					<div class="col-md-6 col-sm-12 text-right">
						<div class="dropdown">
							<a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
								Juillet 2019
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<a class="dropdown-item" href="#">Export List</a>
								<a class="dropdown-item" href="#">Policies</a>
								<a class="dropdown-item" href="#">View Assets</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Default Basic Forms Start -->
			<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">

				<!-- FORMULAIRE -->
				<!-- FORMULAIRE -->
				<!-- FORMULAIRE -->
				<form id="update_account_form">
					<input type="hidden" id="account_id" name="id" value="<?= $datas[2][0]->getId(); ?>">
					<div class="form-group col-md-4 alert alert-danger">
						<label for="inputActive">Compte actif ?</label>
						<select id="inputActive" class="form-control" name="activate">
							<?php if (($datas[2][0]->getActivate()) == "true") : ?>
								<option value="true" selected>Oui</option>
								<option value="false">Non</option>
							<?php else : ?>
								<option value="true">Oui</option>
								<option value="false" selected>Non</option>
							<?php endif; ?>
						</select>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 col-form-label">Nom</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" placeholder="Cash" name="Name" value="<?= $datas[2][0]->getName(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 col-form-label">Prenom</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" placeholder="Johny" name="FirstName" value="<?= $datas[2][0]->getFirstName(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 col-form-label">Birthday</label>
						<div class="form-group col-md-10">
							<input class="form-control date-picker" placeholder="Select Date" type="text" name="Birthday" value="<?= $datas[2][0]->getBirthday(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 col-form-label">Email</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" placeholder="bootstrap@example.com" type="email" name="Email" value="<?= $datas[2][0]->getEmail(); ?>">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-12 col-md-2 col-form-label">Password</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" placeholder="password" type="password" name="Password" value="<?= $datas[2][0]->getPassword(); ?>">
						</div>
					</div> <br>

					<div class="form-row">
						<div class="form-group col-md-2">
							<label for="inputNumber">Numéro</label>
							<input type="text" class="form-control" id="inputNumber" name="number" value="<?= $datas[2][1]->getNumber(); ?>">
						</div>
						<div class="form-group col-md-4">
							<label for="inputState">Type</label>
							<select id="inputState" class="form-control" name="Type_id">
								<option value="<?= $datas[2][1]->getType_id(); ?>"><?= $datas[1][$datas[2][1]->getType_id() - 1]->getName(); ?></option>
								<?php foreach ($datas[1] as $data) : ?>
									<option value="<?= $data->getId(); ?>"><?= $data->getName(); ?></option>
								<?php endforeach; ?>
							</select>
						</div>


						<div class="form-group col-md-6">
							<label for="inputCity">nom</label>
							<input type="text" class="form-control" id="nomRue" placeholder="adresse" name="name" value="<?= $datas[2][1]->getName(); ?>">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="inputZip">Code postal</label>
							<input type="text" class="form-control" id="inputZip" pattern="[0-9]{5}" name="zipcode" value="<?= $datas[2][1]->getZipcode(); ?>">
						</div>
						<div class="form-group col-md-4">
							<label for="inputCity">Ville</label>
							<input type="text" class="form-control" id="inputCity" name="city" value="<?= $datas[2][1]->getCity(); ?>">
						</div>
						<div class="form-group col-md-4">
							<label for="inputCity">Pays</label>
							<input type="text" class="form-control" id="country" placeholder="Pays" name="country" value="<?= $datas[2][1]->getCountry(); ?>">
						</div>
						<div class="form-group col-md-5">
							<label>Role</label>
							<select class="custom-select col-12" name="Role_id" id="Role_id" onchange="test()">
								<option value="<?= $datas[2][0]->getRole_id(); ?>"><?= ($datas[0][$datas[2][0]->getRole_id() - 1]->getDesigniation()); ?></option>
							</select>
						</div>
					</div>



					<div id="response"></div>
					<!-- BUTTON SUBMIT -->
					<button id="button_update_account" type="button" class="btn btn-primary">Enregistrer</button>
					<!-- END BUTTON SUBMIT -->
			</div>
			</form>


		</div>

	</div>
	<?php include('include/footer.php'); ?>
</div>
</div>
<?php include('include/script.php'); ?>
</body>

</html>