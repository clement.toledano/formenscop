<!DOCTYPE html>
<html>

<head>
	<?php include('include/head.php'); ?>
</head>

<body>
	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Edition de role</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/home">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Edition de role</li>
								</ol>
							</nav>
						</div>
						<div class="col-md-6 col-sm-12 text-right">
							<div class="dropdown">
								<a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
									Juillet 2019
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="#">Export List</a>
									<a class="dropdown-item" href="#">Policies</a>
									<a class="dropdown-item" href="#">View Assets</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Default Basic Forms Start -->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">

					<form id="update_role_form">
						<h3>Role</h3><br><br>
						<input type="hidden" id="role_id" name="id" value="<?= $datas->getId(); ?>">

						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Désignation </label>
							<div class="col-sm-12 col-md-10">
								<input value="<?= $datas->getDesigniation(); ?>" class="form-control" type="text" placeholder="name module" name="designiation">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Description</label>
							<div class="form-group col-md-10">
								<input value="<?= $datas->getDescription(); ?>" class="form-control" placeholder="description module" type="text" name="description">
							</div>
						</div>
						<div id="response"></div>

						<br>

						<button id="button_update_role" type="button" class="btn btn-primary" name="register">Enregistrer</button>
					</form>
					<form id="delete_role_form">
					<input type="hidden" id="role_id" name="id" value="<?= $datas->getId(); ?>">

					<button id="button_delete_role" type="button" class="btn btn-danger" name="delete" >Supprimer</button>
					</form>
				</div>
				<!-- Default Basic Forms End -->
			</div>


			<!-- Fin requete insertion dans la base de donnée -->

			<?php include('include/footer.php'); ?>
		</div>
	</div>
	<?php include('include/script.php'); ?>
</body>

</html>