<div class="left-side-bar">
	<div class="brand-logo">
		<a href="/home">
		
		<i style="color: green" class="fas fa-feather-alt"> FORMENSCOP </i>
		
	</a>
</div>
<div class="menu-block customscroll">
	<div class="sidebar-menu">
		<ul id="accordion-menu">
			
			<li class="dropdown">
				<a href="/home" class="dropdown-toggle no-arrow">
				<span class="fa fa-home"></span><span class="mtext">Home</span>
			</a>
		</li>
				
		<!-- ajout sidebar -->
		<li>
			<a href="calendar" class="dropdown-toggle no-arrow">
				<span class="fa fa-calendar-o"></span><span class="mtext">Calendar</span>
			</a>
		</li>
		<li>
			<a href="/messagerie" class="dropdown-toggle no-arrow">
				<span class="fa fa-envelope"></span><span class="mtext">Message<span class="fi-burst-new text-danger new"></span></span>
			</a>
		</li>
		<li class="dropdown">
					<a href="/create-a-account" class="dropdown-toggle no-arrow">
						<span class="fa fa-user-plus"></span><span class="mtext">Create a account</span>
					</a>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="fa fa-user"></span><span class="mtext">Gestion de Compte</span>
					</a>
					<ul class="submenu">
						<?php

						use BWB\Framework\mvc\dao\DAORole;

						$roles = (new DAORole())->getAllRoleUsed();
						foreach ($roles as $role) : ?>
							<li><a href="/account?role=<?= $role->getDesigniation() ?>"><?= $role->getDesigniation() ?></a></li>
						<?php endforeach; ?>


					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="fa fa-pencil"></span><span class="mtext">Gestion des Roles</span>
					</a>
					<ul class="submenu">
						<li><a href="/role">Liste des roles</a></li>
						<li><a href="/create-a-role">Création de role</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="fa fa-book"></span><span class="mtext">Gestion des Congés</span>
					</a>
					<ul class="submenu">
						<li><a href="/holiday-employees">Liste des congés</a></li>
						<li><a href="/holiday-requests">Demande de congés</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="fa fa-table"></span><span class="mtext">Gestion formations</span>
					</a>
					<ul class="submenu">
						<li><a href="/trainings">liste des formations</a></li>
						<li><a href="/create-a-training">Creation de formation</a></li>
						<li><a href="/modules">liste des modules</a></li>
						<li><a href="/create-a-module">Création de module</a></li>

					</ul>
				</li>
				<li>
					<a href="/listeAdvertisments" class="dropdown-toggle no-arrow">
						<span class="fa fa-table"></span><span class="mtext">Liste Annonces</span>
					</a>
				</li>



				<!-- Fin modifications -->
				<!-- Fin modifications -->
				<!-- Fin modifications -->
				<!-- Fin modifications -->
				<br>
				<br>
				<br>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="fa fa-home"></span><span class="mtext">Home</span>
					</a>
					<ul class="submenu">
						<li><a href="index.php">Dashboard style 1</a></li>

					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="fa fa-pencil"></span><span class="mtext">Forms</span>
					</a>
					<ul class="submenu">
						<li><a href="form-basic.php">Form Basic</a></li>
						<li><a href="advanced-components.php">Advanced Components</a></li>
						<li><a href="form-wizard.php">Form Wizard</a></li>
						<li><a href="html5-editor.php">HTML5 Editor</a></li>
						<li><a href="form-pickers.php">Form Pickers</a></li>
						<li><a href="image-cropper.php">Image Cropper</a></li>
						<li><a href="image-dropzone.php">Image Dropzone</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="fa fa-table"></span><span class="mtext">Tables</span>
					</a>
					<ul class="submenu">
						<li><a href="basic-table.php">Basic Tables</a></li>
						<li><a href="datatable.php">DataTables</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="fa fa-desktop"></span><span class="mtext"> UI Elements </span>
					</a>
					<ul class="submenu">
						<li><a href="ui-buttons.php">Buttons</a></li>
						<li><a href="ui-cards.php">Cards</a></li>
						<li><a href="ui-cards-hover.php">Cards Hover</a></li>
						<li><a href="ui-modals.php">Modals</a></li>
						<li><a href="ui-tabs.php">Tabs</a></li>
						<li><a href="ui-tooltip-popover.php">Tooltip &amp; Popover</a></li>
						<li><a href="ui-sweet-alert.php">Sweet Alert</a></li>
						<li><a href="ui-notification.php">Notification</a></li>
						<li><a href="ui-timeline.php">Timeline</a></li>
						<li><a href="ui-progressbar.php">Progressbar</a></li>
						<li><a href="ui-typography.php">Typography</a></li>
						<li><a href="ui-list-group.php">List group</a></li>
						<li><a href="ui-range-slider.php">Range slider</a></li>
						<li><a href="ui-carousel.php">Carousel</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="fa fa-paint-brush"></span><span class="mtext">Icons</span>
					</a>
					<ul class="submenu">
						<li><a href="font-awesome.php">FontAwesome Icons</a></li>
						<li><a href="foundation.php">Foundation Icons</a></li>
						<li><a href="ionicons.php">Ionicons Icons</a></li>
						<li><a href="themify.php">Themify Icons</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="fa fa-plug"></span><span class="mtext">Additional Pages</span>
					</a>
					<ul class="submenu">
						<li><a href="video-player.php">Video Player</a></li>
						<li><a href="login.php">Login</a></li>
						<li><a href="forgot-password.php">Forgot Password</a></li>
						<li><a href="reset-password.php">Reset Password</a></li>
						<li><a href="403.php">403</a></li>
						<li><a href="404.php">404</a></li>
						<li><a href="500.php">500</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="fa fa-clone"></span><span class="mtext">Extra Pages</span>
					</a>
					<ul class="submenu">
						<li><a href="blank.php">Blank</a></li>
						<li><a href="contact-directory.php">Contact Directory</a></li>
						<li><a href="blog.php">Blog</a></li>
						<li><a href="blog-detail.php">Blog Detail</a></li>
						<li><a href="product.php">Product</a></li>
						<li><a href="product-detail.php">Product Detail</a></li>
						<li><a href="faq.php">FAQ</a></li>
						<li><a href="profile.php">Profile</a></li>
						<li><a href="gallery.php">Gallery</a></li>

					</ul>
				</li>



			</ul>
		</div>
	</div>
</div>